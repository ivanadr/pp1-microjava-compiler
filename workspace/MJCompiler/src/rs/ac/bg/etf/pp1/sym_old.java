package rs.ac.bg.etf.pp1;

public class sym_old {
	public static final int PROG = 1;
	public static final int PRINT = 2;
	public static final int RETURN = 3;
	
	// Identifiers
	public static final int IDENT = 4;
	
	// Constants
	public static final int NUMBER = 5;
	
	// Operators
	public static final int PLUS = 6;
	public static final int EQUAL = 7;
	public static final int COMMA = 8;
	public static final int SEMI = 9;
	public static final int LPAREN = 10;
	public static final int RPAREN = 11;
	public static final int LBRACE = 12;
	public static final int RBRACE = 13;
	
	public static final int EOF = 14;
	public static final int VOID = 15;
	
	public static final int READ = 16;
	public static final int NEW = 17;
	public static final int CONST = 18;
	public static final int BOOLEAN = 19;
	public static final int BREAK = 20;

	public static final int INC = 21;
	public static final int DEC = 22;
	public static final int LSQUARE = 23;
	public static final int RSQUARE = 24;
	
	public static final int MINUS = 25;
	public static final int MUL = 26;
	public static final int DIV = 27;
	public static final int MOD = 28;	
	public static final int DOT = 29;
	public static final int CHAR = 30;
	public static final int ENUM = 31;
}
