// generated with ast extension for cup
// version 0.8
// 5/1/2019 20:58:52


package rs.ac.bg.etf.pp1.ast;

public class GlobalVarDeclItem extends GlobalVarDeclList {

    private GlobalVarDeclDef GlobalVarDeclDef;

    public GlobalVarDeclItem (GlobalVarDeclDef GlobalVarDeclDef) {
        this.GlobalVarDeclDef=GlobalVarDeclDef;
        if(GlobalVarDeclDef!=null) GlobalVarDeclDef.setParent(this);
    }

    public GlobalVarDeclDef getGlobalVarDeclDef() {
        return GlobalVarDeclDef;
    }

    public void setGlobalVarDeclDef(GlobalVarDeclDef GlobalVarDeclDef) {
        this.GlobalVarDeclDef=GlobalVarDeclDef;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(GlobalVarDeclDef!=null) GlobalVarDeclDef.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(GlobalVarDeclDef!=null) GlobalVarDeclDef.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(GlobalVarDeclDef!=null) GlobalVarDeclDef.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("GlobalVarDeclItem(\n");

        if(GlobalVarDeclDef!=null)
            buffer.append(GlobalVarDeclDef.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [GlobalVarDeclItem]");
        return buffer.toString();
    }
}
