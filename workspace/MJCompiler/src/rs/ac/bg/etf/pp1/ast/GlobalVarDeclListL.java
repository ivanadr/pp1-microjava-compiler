// generated with ast extension for cup
// version 0.8
// 5/1/2019 20:58:52


package rs.ac.bg.etf.pp1.ast;

public class GlobalVarDeclListL extends GlobalVarDeclList {

    private GlobalVarDeclList GlobalVarDeclList;
    private GlobalVarDeclDef GlobalVarDeclDef;

    public GlobalVarDeclListL (GlobalVarDeclList GlobalVarDeclList, GlobalVarDeclDef GlobalVarDeclDef) {
        this.GlobalVarDeclList=GlobalVarDeclList;
        if(GlobalVarDeclList!=null) GlobalVarDeclList.setParent(this);
        this.GlobalVarDeclDef=GlobalVarDeclDef;
        if(GlobalVarDeclDef!=null) GlobalVarDeclDef.setParent(this);
    }

    public GlobalVarDeclList getGlobalVarDeclList() {
        return GlobalVarDeclList;
    }

    public void setGlobalVarDeclList(GlobalVarDeclList GlobalVarDeclList) {
        this.GlobalVarDeclList=GlobalVarDeclList;
    }

    public GlobalVarDeclDef getGlobalVarDeclDef() {
        return GlobalVarDeclDef;
    }

    public void setGlobalVarDeclDef(GlobalVarDeclDef GlobalVarDeclDef) {
        this.GlobalVarDeclDef=GlobalVarDeclDef;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(GlobalVarDeclList!=null) GlobalVarDeclList.accept(visitor);
        if(GlobalVarDeclDef!=null) GlobalVarDeclDef.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(GlobalVarDeclList!=null) GlobalVarDeclList.traverseTopDown(visitor);
        if(GlobalVarDeclDef!=null) GlobalVarDeclDef.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(GlobalVarDeclList!=null) GlobalVarDeclList.traverseBottomUp(visitor);
        if(GlobalVarDeclDef!=null) GlobalVarDeclDef.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("GlobalVarDeclListL(\n");

        if(GlobalVarDeclList!=null)
            buffer.append(GlobalVarDeclList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(GlobalVarDeclDef!=null)
            buffer.append(GlobalVarDeclDef.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [GlobalVarDeclListL]");
        return buffer.toString();
    }
}
