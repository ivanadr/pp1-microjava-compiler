// generated with ast extension for cup
// version 0.8
// 5/1/2019 20:58:52


package rs.ac.bg.etf.pp1.ast;

public class VarDeclItem extends VarDeclList {

    private VarDeclDef VarDeclDef;

    public VarDeclItem (VarDeclDef VarDeclDef) {
        this.VarDeclDef=VarDeclDef;
        if(VarDeclDef!=null) VarDeclDef.setParent(this);
    }

    public VarDeclDef getVarDeclDef() {
        return VarDeclDef;
    }

    public void setVarDeclDef(VarDeclDef VarDeclDef) {
        this.VarDeclDef=VarDeclDef;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(VarDeclDef!=null) VarDeclDef.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(VarDeclDef!=null) VarDeclDef.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(VarDeclDef!=null) VarDeclDef.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("VarDeclItem(\n");

        if(VarDeclDef!=null)
            buffer.append(VarDeclDef.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [VarDeclItem]");
        return buffer.toString();
    }
}
