// generated with ast extension for cup
// version 0.8
// 5/1/2019 20:58:52


package rs.ac.bg.etf.pp1.ast;

public class VarDeclDefineSingle extends VarDeclDef {

    private String name;

    public VarDeclDefineSingle (String name) {
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name=name;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("VarDeclDefineSingle(\n");

        buffer.append(" "+tab+name);
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [VarDeclDefineSingle]");
        return buffer.toString();
    }
}
