// generated with ast extension for cup
// version 0.8
// 5/1/2019 20:58:52


package rs.ac.bg.etf.pp1.ast;

public abstract class VisitorAdaptor implements Visitor { 

    public void visit(Mulop Mulop) { }
    public void visit(GlobalVarDeclDef GlobalVarDeclDef) { }
    public void visit(StatementList StatementList) { }
    public void visit(Addop Addop) { }
    public void visit(GlobalDecls GlobalDecls) { }
    public void visit(EnumDeclList EnumDeclList) { }
    public void visit(Factor Factor) { }
    public void visit(Designator Designator) { }
    public void visit(Term Term) { }
    public void visit(ConstDeclList ConstDeclList) { }
    public void visit(GlobalDeclList GlobalDeclList) { }
    public void visit(VarDeclList VarDeclList) { }
    public void visit(VarMethodDeclList VarMethodDeclList) { }
    public void visit(ConstVal ConstVal) { }
    public void visit(Expr Expr) { }
    public void visit(VarDeclDef VarDeclDef) { }
    public void visit(DesignatorStatement DesignatorStatement) { }
    public void visit(Assignement Assignement) { }
    public void visit(Statement Statement) { }
    public void visit(Des Des) { }
    public void visit(GlobalVarDeclList GlobalVarDeclList) { }
    public void visit(EnumDeclDef EnumDeclDef) { }
    public void visit(Mulop_Mod Mulop_Mod) { visit(); }
    public void visit(Mulop_Div Mulop_Div) { visit(); }
    public void visit(Mulop_Mul Mulop_Mul) { visit(); }
    public void visit(Addop_Minus Addop_Minus) { visit(); }
    public void visit(Addop_Plus Addop_Plus) { visit(); }
    public void visit(ArrayName ArrayName) { visit(); }
    public void visit(DesArr DesArr) { visit(); }
    public void visit(DesEnum DesEnum) { visit(); }
    public void visit(DesIdent DesIdent) { visit(); }
    public void visit(FactorDesign FactorDesign) { visit(); }
    public void visit(FactorNew FactorNew) { visit(); }
    public void visit(FactorNewArr FactorNewArr) { visit(); }
    public void visit(FactorConst FactorConst) { visit(); }
    public void visit(FactorExpr FactorExpr) { visit(); }
    public void visit(TermMulop TermMulop) { visit(); }
    public void visit(TermFact TermFact) { visit(); }
    public void visit(NegTermExpr NegTermExpr) { visit(); }
    public void visit(TermExpr TermExpr) { visit(); }
    public void visit(AddopExp AddopExp) { visit(); }
    public void visit(StatPrintParam StatPrintParam) { visit(); }
    public void visit(StatPrint StatPrint) { visit(); }
    public void visit(StatRead StatRead) { visit(); }
    public void visit(StatementDes StatementDes) { visit(); }
    public void visit(AssignementExprError AssignementExprError) { visit(); }
    public void visit(AssignementExpr AssignementExpr) { visit(); }
    public void visit(DesignStatDec DesignStatDec) { visit(); }
    public void visit(DesignStatInc DesignStatInc) { visit(); }
    public void visit(DesignStatAssignment DesignStatAssignment) { visit(); }
    public void visit(StatementList_Eps StatementList_Eps) { visit(); }
    public void visit(StatementList_NoEps StatementList_NoEps) { visit(); }
    public void visit(VarMethodDeclList_Eps VarMethodDeclList_Eps) { visit(); }
    public void visit(VarMethodDeclList_NoEps VarMethodDeclList_NoEps) { visit(); }
    public void visit(VoidableType VoidableType) { visit(); }
    public void visit(MethodSignature MethodSignature) { visit(); }
    public void visit(MethodDecl MethodDecl) { visit(); }
    public void visit(EnumDeclDefVal EnumDeclDefVal) { visit(); }
    public void visit(EnumDeclDefNoVal EnumDeclDefNoVal) { visit(); }
    public void visit(EnumDeclItem EnumDeclItem) { visit(); }
    public void visit(EnumDeclListL EnumDeclListL) { visit(); }
    public void visit(EnumName EnumName) { visit(); }
    public void visit(EnumDecl EnumDecl) { visit(); }
    public void visit(VarDeclDefineArray VarDeclDefineArray) { visit(); }
    public void visit(VarDeclDefineSingle VarDeclDefineSingle) { visit(); }
    public void visit(VarDeclItem VarDeclItem) { visit(); }
    public void visit(VarDeclListL VarDeclListL) { visit(); }
    public void visit(VarDecl VarDecl) { visit(); }
    public void visit(GlobalVarDeclDefError GlobalVarDeclDefError) { visit(); }
    public void visit(GlobalVarDeclDefOk GlobalVarDeclDefOk) { visit(); }
    public void visit(GlobalVarDeclItem GlobalVarDeclItem) { visit(); }
    public void visit(GlobalVarDeclListL GlobalVarDeclListL) { visit(); }
    public void visit(GlobalVarDecl GlobalVarDecl) { visit(); }
    public void visit(Type Type) { visit(); }
    public void visit(ConstChar ConstChar) { visit(); }
    public void visit(ConstBool ConstBool) { visit(); }
    public void visit(ConstNum ConstNum) { visit(); }
    public void visit(ConstDef ConstDef) { visit(); }
    public void visit(ConstDeclItem ConstDeclItem) { visit(); }
    public void visit(ConstDeclListL ConstDeclListL) { visit(); }
    public void visit(ConstDecl ConstDecl) { visit(); }
    public void visit(GlobalDeclEnum GlobalDeclEnum) { visit(); }
    public void visit(GlobalDeclVar GlobalDeclVar) { visit(); }
    public void visit(GlobalDeclConst GlobalDeclConst) { visit(); }
    public void visit(GlobalDeclListEps GlobalDeclListEps) { visit(); }
    public void visit(GlobalDeclListL GlobalDeclListL) { visit(); }
    public void visit(ProgName ProgName) { visit(); }
    public void visit(Program Program) { visit(); }


    public void visit() { }
}
