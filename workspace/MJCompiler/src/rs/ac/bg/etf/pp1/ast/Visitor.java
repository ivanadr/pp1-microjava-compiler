// generated with ast extension for cup
// version 0.8
// 5/1/2019 20:58:52


package rs.ac.bg.etf.pp1.ast;

public interface Visitor { 

    public void visit(Mulop Mulop);
    public void visit(GlobalVarDeclDef GlobalVarDeclDef);
    public void visit(StatementList StatementList);
    public void visit(Addop Addop);
    public void visit(GlobalDecls GlobalDecls);
    public void visit(EnumDeclList EnumDeclList);
    public void visit(Factor Factor);
    public void visit(Designator Designator);
    public void visit(Term Term);
    public void visit(ConstDeclList ConstDeclList);
    public void visit(GlobalDeclList GlobalDeclList);
    public void visit(VarDeclList VarDeclList);
    public void visit(VarMethodDeclList VarMethodDeclList);
    public void visit(ConstVal ConstVal);
    public void visit(Expr Expr);
    public void visit(VarDeclDef VarDeclDef);
    public void visit(DesignatorStatement DesignatorStatement);
    public void visit(Assignement Assignement);
    public void visit(Statement Statement);
    public void visit(Des Des);
    public void visit(GlobalVarDeclList GlobalVarDeclList);
    public void visit(EnumDeclDef EnumDeclDef);
    public void visit(Mulop_Mod Mulop_Mod);
    public void visit(Mulop_Div Mulop_Div);
    public void visit(Mulop_Mul Mulop_Mul);
    public void visit(Addop_Minus Addop_Minus);
    public void visit(Addop_Plus Addop_Plus);
    public void visit(ArrayName ArrayName);
    public void visit(DesArr DesArr);
    public void visit(DesEnum DesEnum);
    public void visit(DesIdent DesIdent);
    public void visit(FactorDesign FactorDesign);
    public void visit(FactorNew FactorNew);
    public void visit(FactorNewArr FactorNewArr);
    public void visit(FactorConst FactorConst);
    public void visit(FactorExpr FactorExpr);
    public void visit(TermMulop TermMulop);
    public void visit(TermFact TermFact);
    public void visit(NegTermExpr NegTermExpr);
    public void visit(TermExpr TermExpr);
    public void visit(AddopExp AddopExp);
    public void visit(StatPrintParam StatPrintParam);
    public void visit(StatPrint StatPrint);
    public void visit(StatRead StatRead);
    public void visit(StatementDes StatementDes);
    public void visit(AssignementExprError AssignementExprError);
    public void visit(AssignementExpr AssignementExpr);
    public void visit(DesignStatDec DesignStatDec);
    public void visit(DesignStatInc DesignStatInc);
    public void visit(DesignStatAssignment DesignStatAssignment);
    public void visit(StatementList_Eps StatementList_Eps);
    public void visit(StatementList_NoEps StatementList_NoEps);
    public void visit(VarMethodDeclList_Eps VarMethodDeclList_Eps);
    public void visit(VarMethodDeclList_NoEps VarMethodDeclList_NoEps);
    public void visit(VoidableType VoidableType);
    public void visit(MethodSignature MethodSignature);
    public void visit(MethodDecl MethodDecl);
    public void visit(EnumDeclDefVal EnumDeclDefVal);
    public void visit(EnumDeclDefNoVal EnumDeclDefNoVal);
    public void visit(EnumDeclItem EnumDeclItem);
    public void visit(EnumDeclListL EnumDeclListL);
    public void visit(EnumName EnumName);
    public void visit(EnumDecl EnumDecl);
    public void visit(VarDeclDefineArray VarDeclDefineArray);
    public void visit(VarDeclDefineSingle VarDeclDefineSingle);
    public void visit(VarDeclItem VarDeclItem);
    public void visit(VarDeclListL VarDeclListL);
    public void visit(VarDecl VarDecl);
    public void visit(GlobalVarDeclDefError GlobalVarDeclDefError);
    public void visit(GlobalVarDeclDefOk GlobalVarDeclDefOk);
    public void visit(GlobalVarDeclItem GlobalVarDeclItem);
    public void visit(GlobalVarDeclListL GlobalVarDeclListL);
    public void visit(GlobalVarDecl GlobalVarDecl);
    public void visit(Type Type);
    public void visit(ConstChar ConstChar);
    public void visit(ConstBool ConstBool);
    public void visit(ConstNum ConstNum);
    public void visit(ConstDef ConstDef);
    public void visit(ConstDeclItem ConstDeclItem);
    public void visit(ConstDeclListL ConstDeclListL);
    public void visit(ConstDecl ConstDecl);
    public void visit(GlobalDeclEnum GlobalDeclEnum);
    public void visit(GlobalDeclVar GlobalDeclVar);
    public void visit(GlobalDeclConst GlobalDeclConst);
    public void visit(GlobalDeclListEps GlobalDeclListEps);
    public void visit(GlobalDeclListL GlobalDeclListL);
    public void visit(ProgName ProgName);
    public void visit(Program Program);

}
