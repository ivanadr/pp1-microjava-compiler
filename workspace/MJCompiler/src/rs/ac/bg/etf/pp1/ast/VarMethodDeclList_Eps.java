// generated with ast extension for cup
// version 0.8
// 5/1/2019 20:58:52


package rs.ac.bg.etf.pp1.ast;

public class VarMethodDeclList_Eps extends VarMethodDeclList {

    public VarMethodDeclList_Eps () {
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("VarMethodDeclList_Eps(\n");

        buffer.append(tab);
        buffer.append(") [VarMethodDeclList_Eps]");
        return buffer.toString();
    }
}
