// generated with ast extension for cup
// version 0.8
// 5/1/2019 20:58:52


package rs.ac.bg.etf.pp1.ast;

public class EnumDeclListL extends EnumDeclList {

    private EnumDeclList EnumDeclList;
    private EnumDeclDef EnumDeclDef;

    public EnumDeclListL (EnumDeclList EnumDeclList, EnumDeclDef EnumDeclDef) {
        this.EnumDeclList=EnumDeclList;
        if(EnumDeclList!=null) EnumDeclList.setParent(this);
        this.EnumDeclDef=EnumDeclDef;
        if(EnumDeclDef!=null) EnumDeclDef.setParent(this);
    }

    public EnumDeclList getEnumDeclList() {
        return EnumDeclList;
    }

    public void setEnumDeclList(EnumDeclList EnumDeclList) {
        this.EnumDeclList=EnumDeclList;
    }

    public EnumDeclDef getEnumDeclDef() {
        return EnumDeclDef;
    }

    public void setEnumDeclDef(EnumDeclDef EnumDeclDef) {
        this.EnumDeclDef=EnumDeclDef;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(EnumDeclList!=null) EnumDeclList.accept(visitor);
        if(EnumDeclDef!=null) EnumDeclDef.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(EnumDeclList!=null) EnumDeclList.traverseTopDown(visitor);
        if(EnumDeclDef!=null) EnumDeclDef.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(EnumDeclList!=null) EnumDeclList.traverseBottomUp(visitor);
        if(EnumDeclDef!=null) EnumDeclDef.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("EnumDeclListL(\n");

        if(EnumDeclList!=null)
            buffer.append(EnumDeclList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(EnumDeclDef!=null)
            buffer.append(EnumDeclDef.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [EnumDeclListL]");
        return buffer.toString();
    }
}
