// generated with ast extension for cup
// version 0.8
// 5/1/2019 20:58:52


package rs.ac.bg.etf.pp1.ast;

public class DesEnum extends Designator {

    private String I1;
    private String ided;

    public DesEnum (String I1, String ided) {
        this.I1=I1;
        this.ided=ided;
    }

    public String getI1() {
        return I1;
    }

    public void setI1(String I1) {
        this.I1=I1;
    }

    public String getIded() {
        return ided;
    }

    public void setIded(String ided) {
        this.ided=ided;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("DesEnum(\n");

        buffer.append(" "+tab+I1);
        buffer.append("\n");

        buffer.append(" "+tab+ided);
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [DesEnum]");
        return buffer.toString();
    }
}
