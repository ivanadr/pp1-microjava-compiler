// generated with ast extension for cup
// version 0.8
// 5/1/2019 20:58:52


package rs.ac.bg.etf.pp1.ast;

public class EnumDeclItem extends EnumDeclList {

    private EnumDeclDef EnumDeclDef;

    public EnumDeclItem (EnumDeclDef EnumDeclDef) {
        this.EnumDeclDef=EnumDeclDef;
        if(EnumDeclDef!=null) EnumDeclDef.setParent(this);
    }

    public EnumDeclDef getEnumDeclDef() {
        return EnumDeclDef;
    }

    public void setEnumDeclDef(EnumDeclDef EnumDeclDef) {
        this.EnumDeclDef=EnumDeclDef;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(EnumDeclDef!=null) EnumDeclDef.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(EnumDeclDef!=null) EnumDeclDef.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(EnumDeclDef!=null) EnumDeclDef.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("EnumDeclItem(\n");

        if(EnumDeclDef!=null)
            buffer.append(EnumDeclDef.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [EnumDeclItem]");
        return buffer.toString();
    }
}
