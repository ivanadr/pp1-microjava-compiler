// generated with ast extension for cup
// version 0.8
// 5/1/2019 20:58:52


package rs.ac.bg.etf.pp1.ast;

public class VarMethodDeclList_NoEps extends VarMethodDeclList {

    private VarMethodDeclList VarMethodDeclList;
    private VarDecl VarDecl;

    public VarMethodDeclList_NoEps (VarMethodDeclList VarMethodDeclList, VarDecl VarDecl) {
        this.VarMethodDeclList=VarMethodDeclList;
        if(VarMethodDeclList!=null) VarMethodDeclList.setParent(this);
        this.VarDecl=VarDecl;
        if(VarDecl!=null) VarDecl.setParent(this);
    }

    public VarMethodDeclList getVarMethodDeclList() {
        return VarMethodDeclList;
    }

    public void setVarMethodDeclList(VarMethodDeclList VarMethodDeclList) {
        this.VarMethodDeclList=VarMethodDeclList;
    }

    public VarDecl getVarDecl() {
        return VarDecl;
    }

    public void setVarDecl(VarDecl VarDecl) {
        this.VarDecl=VarDecl;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(VarMethodDeclList!=null) VarMethodDeclList.accept(visitor);
        if(VarDecl!=null) VarDecl.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(VarMethodDeclList!=null) VarMethodDeclList.traverseTopDown(visitor);
        if(VarDecl!=null) VarDecl.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(VarMethodDeclList!=null) VarMethodDeclList.traverseBottomUp(visitor);
        if(VarDecl!=null) VarDecl.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("VarMethodDeclList_NoEps(\n");

        if(VarMethodDeclList!=null)
            buffer.append(VarMethodDeclList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(VarDecl!=null)
            buffer.append(VarDecl.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [VarMethodDeclList_NoEps]");
        return buffer.toString();
    }
}
