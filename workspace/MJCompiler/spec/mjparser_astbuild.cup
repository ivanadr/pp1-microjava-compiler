package rs.ac.bg.etf.pp1;

import java_cup.runtime.*;
import org.apache.log4j.*;

// import java.io.*;
import rs.ac.bg.etf.pp1.ast.*;


parser code {:
	
	boolean errorDetected = false;
	
	Logger log = Logger.getLogger(getClass());
   
   
    // slede redefinisani metodi za prijavu gresaka radi izmene teksta poruke
     
    public void report_fatal_error(String message, Object info) throws java.lang.Exception {
      done_parsing();
      report_error(message, info);
    }
  
    public void syntax_error(Symbol cur_token) {
        report_error("\nSintaksna greska", cur_token);
    }
  
    public void unrecovered_syntax_error(Symbol cur_token) throws java.lang.Exception {     
        report_fatal_error("Fatalna greska, parsiranje se ne moze nastaviti", cur_token);
    }

    public void report_error(String message, Object info) {
    	errorDetected = true;
    	StringBuilder msg = new StringBuilder(message); 
    	if (info instanceof Symbol)
            msg.append (" na liniji ").append(((Symbol)info).left);        
        log.error(msg.toString());
      
    }
    
    public void report_info(String message, Object info) {
    	StringBuilder msg = new StringBuilder(message); 
    	if (info instanceof Symbol)
            msg.append (" na liniji ").append(((Symbol)info).left);
        log.info(msg.toString());
    }
    
:}

scan with {:
	Symbol s = this.getScanner().next_token();
	if (s != null && s.value != null) 
		log.info(s.toString() + " " + s.value.toString());
	return s;
:}


terminal PROG, READ, PRINT, NEW, VOID, RETURN, CONST, ENUM;
terminal SEMI, EQUAL, COMMA, LPAREN, RPAREN, LSQUARE, RSQUARE, LBRACE, RBRACE, DOT;
terminal INC, DEC, MINUS, PLUS, MUL, DIV, MOD;
terminal Integer NUMBER;
terminal String IDENT;
terminal Character CHAR;
terminal Boolean BOOLEAN;

nonterminal GlobalDeclList GlobalDeclList;
nonterminal GlobalDecls GlobalDecls;
nonterminal GlobalVarDecl GlobalVarDecl;
nonterminal GlobalVarDeclList GlobalVarDeclList;
nonterminal GlobalVarDeclDef GlobalVarDeclDef;
nonterminal ConstDeclList ConstDeclList;
nonterminal VarDecl VarDecl;
nonterminal VarDeclList VarDeclList;
nonterminal EnumDeclList EnumDeclList;
nonterminal StatementList StatementList;
nonterminal Statement Statement;
nonterminal DesignatorStatement DesignatorStatement;
nonterminal Mulop Mulop;
nonterminal Addop Addop;
nonterminal MethodDecl MethodDecl;
nonterminal VarMethodDeclList VarMethodDeclList;

nonterminal rs.etf.pp1.symboltable.concepts.Obj  ProgName, Program, ConstVal, ConstDef, ConstDecl, VoidableType, MethodSignature, Designator, Des;
nonterminal rs.etf.pp1.symboltable.concepts.Obj  EnumDecl, EnumDeclDef,  EnumName, ArrayName, VarDeclDef;
nonterminal rs.etf.pp1.symboltable.concepts.Obj  Expr, Assignement, Term, Factor;
nonterminal rs.etf.pp1.symboltable.concepts.Struct Type;

Program ::= (Program) PROG ProgName:p GlobalDeclList:G1 LBRACE MethodDecl:M2 RBRACE {: RESULT=new Program(p, G1, M2); RESULT.setLine(pleft); :} ;

ProgName ::= (ProgName) IDENT:progName {: RESULT=new ProgName(progName); RESULT.setLine(progNameleft); :} ;

GlobalDeclList ::= (GlobalDeclListL) GlobalDeclList:G1 GlobalDecls:G2 {: RESULT=new GlobalDeclListL(G1, G2); RESULT.setLine(G1left); :}
			 |
			 (GlobalDeclListEps) {: RESULT=new GlobalDeclListEps(); :} /*epsilon*/
			 ;

GlobalDecls ::= (GlobalDeclConst) ConstDecl:C1 {: RESULT=new GlobalDeclConst(C1); RESULT.setLine(C1left); :} 
		  		| 
		  		(GlobalDeclVar) GlobalVarDecl:G1 {: RESULT=new GlobalDeclVar(G1); RESULT.setLine(G1left); :} 
		  		|
		  		(GlobalDeclEnum) EnumDecl:E1 {: RESULT=new GlobalDeclEnum(E1); RESULT.setLine(E1left); :}
		  		;
		  
		 
ConstDecl ::= (ConstDecl) CONST Type:T1 ConstDeclList:C2 SEMI {: RESULT=new ConstDecl(T1, C2); RESULT.setLine(T1left); :} ;

ConstDeclList ::= (ConstDeclListL) ConstDeclList:C1 COMMA ConstDef:C2 {: RESULT=new ConstDeclListL(C1, C2); RESULT.setLine(C1left); :}
			   	  |
			      (ConstDeclItem) ConstDef:C1 {: RESULT=new ConstDeclItem(C1); RESULT.setLine(C1left); :}
			   	  ;
			   	  
ConstDef ::= (ConstDef) IDENT:constName EQUAL ConstVal:constValue {: RESULT=new ConstDef(constName, constValue); RESULT.setLine(constNameleft); :};

ConstVal ::= (ConstNum) NUMBER:num {: RESULT=new ConstNum(num); RESULT.setLine(numleft); :}
		     |
		  	 (ConstBool) BOOLEAN:bool {: RESULT=new ConstBool(bool); RESULT.setLine(boolleft); :} 
		  	 |
		  	 (ConstChar) CHAR:chr {: RESULT=new ConstChar(chr); RESULT.setLine(chrleft); :}
		  	 ;

Type ::= (Type) IDENT:typeName {: RESULT=new Type(typeName); RESULT.setLine(typeNameleft); :};

GlobalVarDecl ::= (GlobalVarDecl) Type:T1 GlobalVarDeclList:G2 SEMI {: RESULT=new GlobalVarDecl(T1, G2); RESULT.setLine(T1left); :} ;

GlobalVarDeclList ::= (GlobalVarDeclListL) GlobalVarDeclList:G1 COMMA GlobalVarDeclDef:G2 {: RESULT=new GlobalVarDeclListL(G1, G2); RESULT.setLine(G1left); :}
					  |
					  (GlobalVarDeclItem) GlobalVarDeclDef:G1 {: RESULT=new GlobalVarDeclItem(G1); RESULT.setLine(G1left); :}
					  ;
					  
GlobalVarDeclDef ::= (GlobalVarDeclDefOk) VarDeclDef:V1 {: RESULT=new GlobalVarDeclDefOk(V1); RESULT.setLine(V1left); :}
					 |
					 (GlobalVarDeclDefError) error:e
                     {: parser.report_info("Oporavak od greske u definiciji globalne promenljive na liniji " + eleft, null); :} {: RESULT=new GlobalVarDeclDefError(); :}		
                     ;			  

VarDecl ::= (VarDecl) Type:type VarDeclList:V1 SEMI {: RESULT=new VarDecl(type, V1); RESULT.setLine(typeleft); :};

VarDeclList ::= (VarDeclListL) VarDeclList:V1 COMMA VarDeclDef:V2 {: RESULT=new VarDeclListL(V1, V2); RESULT.setLine(V1left); :}
                |
                (VarDeclItem) VarDeclDef:V1 {: RESULT=new VarDeclItem(V1); RESULT.setLine(V1left); :}
                ;
                
VarDeclDef ::= (VarDeclDefineSingle) IDENT:name {: RESULT=new VarDeclDefineSingle(name); RESULT.setLine(nameleft); :}
               |
               (VarDeclDefineArray) IDENT:name LSQUARE RSQUARE {: RESULT=new VarDeclDefineArray(name); RESULT.setLine(nameleft); :}
               ;
EnumDecl ::= (EnumDecl) ENUM  EnumName:E1 LBRACE EnumDeclList:E2 RBRACE {: RESULT=new EnumDecl(E1, E2); RESULT.setLine(E1left); :};

EnumName ::= (EnumName) IDENT:id {: RESULT=new EnumName(id); RESULT.setLine(idleft); :};

EnumDeclList ::= (EnumDeclListL) EnumDeclList:E1 COMMA EnumDeclDef:E2 {: RESULT=new EnumDeclListL(E1, E2); RESULT.setLine(E1left); :}
				 | 
				 (EnumDeclItem) EnumDeclDef:E1 {: RESULT=new EnumDeclItem(E1); RESULT.setLine(E1left); :}
				 ;

EnumDeclDef ::= (EnumDeclDefNoVal) IDENT:name {: RESULT=new EnumDeclDefNoVal(name); RESULT.setLine(nameleft); :}
				|
				(EnumDeclDefVal) IDENT:name EQUAL NUMBER:num {: RESULT=new EnumDeclDefVal(name, num); RESULT.setLine(nameleft); :}
				;


MethodDecl ::= (MethodDecl) MethodSignature:M1 VarMethodDeclList:V2 LBRACE StatementList:S3 RBRACE {: RESULT=new MethodDecl(M1, V2, S3); RESULT.setLine(M1left); :} ;

MethodSignature ::= (MethodSignature) VoidableType:V1 IDENT:I2 LPAREN RPAREN {: RESULT=new MethodSignature(V1, I2); RESULT.setLine(V1left); :} ;

VoidableType ::= (VoidableType) VOID {: RESULT=new VoidableType(); :};
				 


VarMethodDeclList ::= (VarMethodDeclList_NoEps) VarMethodDeclList:V1 VarDecl:V2 {: RESULT=new VarMethodDeclList_NoEps(V1, V2); RESULT.setLine(V1left); :}
                |
                (VarMethodDeclList_Eps) {: RESULT=new VarMethodDeclList_Eps(); :} /* epsilon */
                ;
StatementList ::= (StatementList_NoEps) StatementList:S1 Statement:S2 {: RESULT=new StatementList_NoEps(S1, S2); RESULT.setLine(S1left); :}
                  |
                  (StatementList_Eps) {: RESULT=new StatementList_Eps(); :} /* epsilon */
                  ;	
				        
DesignatorStatement ::= (DesignStatAssignment) Designator:deq Assignement:A1 {: RESULT=new DesignStatAssignment(deq, A1); RESULT.setLine(deqleft); :}
					    |
						(DesignStatInc) Designator:dinc INC {: RESULT=new DesignStatInc(dinc); RESULT.setLine(dincleft); :}
						|
						(DesignStatDec) Designator:ddec DEC {: RESULT=new DesignStatDec(ddec); RESULT.setLine(ddecleft); :}						
						;

Assignement ::= (AssignementExpr) EQUAL Expr:e {: RESULT=new AssignementExpr(e); RESULT.setLine(eleft); :}
				|
				(AssignementExprError) error:e 
				{: parser.report_info("Oporavak od greske u konstrukciji iskaza dodele na liniji " + eleft, null); :} {: RESULT=new AssignementExprError(); :}		
                ;

Statement ::= (StatementDes) DesignatorStatement:ds SEMI {: RESULT=new StatementDes(ds); RESULT.setLine(dsleft); :}
			  |
			  (StatRead) READ LPAREN Designator:d RPAREN SEMI {: RESULT=new StatRead(d); RESULT.setLine(dleft); :}
			  |
			  (StatPrint) PRINT LPAREN Expr:e RPAREN SEMI {: RESULT=new StatPrint(e); RESULT.setLine(eleft); :}
			  | 
			  (StatPrintParam) PRINT LPAREN Expr:ep COMMA NUMBER:N1 RPAREN SEMI {: RESULT=new StatPrintParam(ep, N1); RESULT.setLine(epleft); :}
			  ;
			
Expr ::= (AddopExp) Expr:te Addop:A1 Term:t {: RESULT=new AddopExp(te, A1, t); RESULT.setLine(teleft); :}
		 | 
		 (TermExpr) Term:t {: RESULT=new TermExpr(t); RESULT.setLine(tleft); :}
		 |
		 (NegTermExpr) MINUS Term:t {: RESULT=new NegTermExpr(t); RESULT.setLine(tleft); :}
		 ;
		
Term ::= (TermFact) Factor:f {: RESULT=new TermFact(f); RESULT.setLine(fleft); :}
		 | 
		 (TermMulop) Term:tr Mulop:M1 Factor:f {: RESULT=new TermMulop(tr, M1, f); RESULT.setLine(trleft); :}
		 ;

Factor ::= (FactorExpr) LPAREN Expr:E1 RPAREN {: RESULT=new FactorExpr(E1); RESULT.setLine(E1left); :}
		   |
		   (FactorConst) ConstVal:C1 {: RESULT=new FactorConst(C1); RESULT.setLine(C1left); :}
		   |
		   (FactorNewArr) NEW Type:t LSQUARE Expr:E1 RSQUARE {: RESULT=new FactorNewArr(t, E1); RESULT.setLine(tleft); :}
		   |
		   (FactorNew) NEW Type:ty {: RESULT=new FactorNew(ty); RESULT.setLine(tyleft); :}
		   |
		   (FactorDesign) Designator:D1 {: RESULT=new FactorDesign(D1); RESULT.setLine(D1left); :}
		   ;
		   


Designator ::=	(DesIdent) IDENT:id {: RESULT=new DesIdent(id); RESULT.setLine(idleft); :}
		|
		(DesEnum) IDENT:I1 DOT IDENT:ided {: RESULT=new DesEnum(I1, ided); RESULT.setLine(I1left); :}
		|
		(DesArr) ArrayName:A1 LSQUARE Expr:e RSQUARE {: RESULT=new DesArr(A1, e); RESULT.setLine(A1left); :}		
		;

ArrayName ::= (ArrayName) IDENT:id {: RESULT=new ArrayName(id); RESULT.setLine(idleft); :} ;			   		

Addop ::= (Addop_Plus) PLUS {: RESULT=new Addop_Plus(); :} 
		  |
		  (Addop_Minus) MINUS {: RESULT=new Addop_Minus(); :}
		  ;
	
Mulop ::= (Mulop_Mul) MUL {: RESULT=new Mulop_Mul(); :}
		  |
		  (Mulop_Div) DIV {: RESULT=new Mulop_Div(); :}
		  |
		  (Mulop_Mod) MOD {: RESULT=new Mulop_Mod(); :}
		  ;	
			

			
