package rs.ac.bg.etf.pp1;

import java_cup.runtime.*;
import org.apache.log4j.*;

// import java.io.*;
import rs.ac.bg.etf.pp1.ast.*;


parser code {:
	
	boolean errorDetected = false;
	
	Logger log = Logger.getLogger(getClass());
   
   
    // slede redefinisani metodi za prijavu gresaka radi izmene teksta poruke
     
    public void report_fatal_error(String message, Object info) throws java.lang.Exception {
      done_parsing();
      report_error(message, info);
    }
  
    public void syntax_error(Symbol cur_token) {
        report_error("\nSintaksna greska", cur_token);
    }
  
    public void unrecovered_syntax_error(Symbol cur_token) throws java.lang.Exception {
        report_fatal_error("Fatalna greska, parsiranje se ne moze nastaviti", cur_token);
    }

    public void report_error(String message, Object info) {
    	errorDetected = true;
    	StringBuilder msg = new StringBuilder(message); 
    	if (info instanceof Symbol)
            msg.append (" na liniji ").append(((Symbol)info).left);
        log.error(msg.toString());
    }
    
    public void report_info(String message, Object info) {
    	StringBuilder msg = new StringBuilder(message); 
    	if (info instanceof Symbol)
            msg.append (" na liniji ").append(((Symbol)info).left);
        log.info(msg.toString());
    }
    
:}

scan with {:
	Symbol s = this.getScanner().next_token();
	if (s != null && s.value != null) 
		log.info(s.toString() + " " + s.value.toString());
	return s;
:}


terminal PROG, READ, PRINT, NEW, VOID, RETURN, CONST, ENUM;
terminal SEMI, EQUAL, COMMA, LPAREN, RPAREN, LSQUARE, RSQUARE, LBRACE, RBRACE, DOT;
terminal INC, DEC, MINUS, PLUS, MUL, DIV, MOD;
terminal Integer NUMBER;
terminal String IDENT;
terminal Character CHAR;
terminal Boolean BOOLEAN;

nonterminal GlobalDeclList, GlobalDecls;
nonterminal GlobalVarDecl, GlobalVarDeclList, GlobalVarDeclDef;
nonterminal ConstDeclList;
nonterminal VarDecl, VarDeclList, VarDeclDef;
nonterminal EnumDecl, EnumDeclList, EnumDeclDef;
nonterminal StatementList, Statement, DesignatorStatement;
nonterminal Mulop, Addop;
nonterminal MethodDecl, VarMethodDeclList;

nonterminal rs.etf.pp1.symboltable.concepts.Obj  ProgName, Program, ConstVal, ConstDef, ConstDecl, VoidableType, MethodSignature, Designator, Des;
nonterminal rs.etf.pp1.symboltable.concepts.Struct Type,  Expr, Assignement, Term, Factor;

Program ::= (Program) PROG ProgName:p GlobalDeclList LBRACE MethodDecl RBRACE ;

ProgName ::= (ProgName) IDENT:progName ;

GlobalDeclList ::= (GlobalDeclListL) GlobalDeclList GlobalDecls
			 |
			 (GlobalDeclListEps) /*epsilon*/
			 ;

GlobalDecls ::= (GlobalDeclConst) ConstDecl 
		  		| 
		  		(GlobalDeclVar) GlobalVarDecl 
		  		|
		  		(GlobalDeclEnum) EnumDecl
		  		;
		  
		 
ConstDecl ::= (ConstDecl) CONST Type ConstDeclList SEMI ;

ConstDeclList ::= (ConstDeclListL) ConstDeclList COMMA ConstDef
			   	  |
			      (ConstDeclItem) ConstDef
			   	  ;
			   	  
ConstDef ::= (ConstDef) IDENT:constName EQUAL ConstVal:constValue;

ConstVal ::= (ConstNum) NUMBER:num
		     |
		  	 (ConstBool) BOOLEAN:bool 
		  	 |
		  	 (ConstChar) CHAR:chr
		  	 ;

Type ::= (Type) IDENT:typeName;

GlobalVarDecl ::= (GlobalVarDecl) Type GlobalVarDeclList SEMI ;

GlobalVarDeclList ::= (GlobalVarDeclListL) GlobalVarDeclList COMMA GlobalVarDeclDef
					  |
					  (GlobalVarDeclItem) GlobalVarDeclDef
					  ;
					  
GlobalVarDeclDef ::= (GlobalVarDeclDefOk) VarDeclDef
					 |
					 (GlobalVarDeclDefError) error:e
                     {: parser.report_info("Oporavak od greske u definiciji globalne promenljive na liniji " + eleft, null); :}		
                     ;			  

VarDecl ::= (VarDecl) Type:type VarDeclList SEMI;

VarDeclList ::= (VarDeclListL) VarDeclList COMMA VarDeclDef
                |
                (VarDeclItem) VarDeclDef
                ;
                
VarDeclDef ::= (VarDeclDefineSingle) IDENT:name
               |
               (VarDeclDefineArray) IDENT:name LSQUARE RSQUARE
               ;
EnumDecl ::= (EnumDecl) ENUM IDENT LBRACE EnumDeclList RBRACE;

EnumDeclList ::= (EnumDeclListL) EnumDeclList COMMA EnumDeclDef
				 | 
				 (EnumDeclItem) EnumDeclDef
				 ;

EnumDeclDef ::= (EnumDeclDefNoVal) IDENT:name
				|
				(EnumDeclDefVal) IDENT:name EQUAL NUMBER:num
				;


MethodDecl ::= (MethodDecl) MethodSignature VarMethodDeclList LBRACE StatementList RBRACE ;

MethodSignature ::= (MethodSignature) VoidableType IDENT LPAREN RPAREN ;

VoidableType ::= (VoidableType) VOID;
				 


VarMethodDeclList ::= (VarMethodDeclList_NoEps) VarMethodDeclList VarDecl
                |
                (VarMethodDeclList_Eps) /* epsilon */
                ;
StatementList ::= (StatementList_NoEps) StatementList Statement
                  |
                  (StatementList_Eps) /* epsilon */
                  ;	
				        
DesignatorStatement ::= (DesignStatAssignment) Designator:deq Assignement
					    |
						(DesignStatInc) Designator:dinc INC
						|
						(DesignStatDec) Designator:ddec DEC						
						;

Assignement ::= (AssignementExpr) EQUAL Expr:e
				|
				(AssignementExprError) error:e 
				{: parser.report_info("Oporavak od greske u konstrukciji iskaza dodele na liniji " + eleft, null); :}		
                ;

Statement ::= (StatementDes) DesignatorStatement:ds SEMI
			  |
			  (StatRead) READ LPAREN Designator:d RPAREN SEMI
			  |
			  (StatPrint) PRINT LPAREN Expr:e RPAREN SEMI
			  | 
			  (StatPrintParam) PRINT LPAREN Expr:ep COMMA NUMBER RPAREN SEMI
			  ;
			
Expr ::= (AddopExp) Expr:te Addop Term:t
		 | 
		 (TermExpr) Term:t
		 |
		 (NegTermExpr) MINUS Term:t
		 ;
		
Term ::= (TermFact) Factor:f
		 | 
		 (TermMulop) Term:tr Mulop Factor:f
		 ;

Factor ::= (FactorExpr) LPAREN Expr RPAREN
		   |
		   (FactorConst) ConstVal
		   |
		   (FactorNewArr) NEW Type:t LSQUARE Expr RSQUARE
		   |
		   (FactorNew) NEW Type:ty
		   |
		   (FactorDesign) Designator
		   ;
		   
Designator ::= (DesignatorIdent) IDENT:id Des;

Des ::=	(DesEnum) DOT IDENT:ided
		|
		(DesArr) LSQUARE Expr:e RSQUARE
		|
		/*epsilon*/
		;
			   		

Addop ::= (Addop) PLUS 
		  |
		  (Subop) MINUS
		  ;
	
Mulop ::= (Mulop) MUL
		  |
		  (Divop) DIV
		  |
		  (Modop) MOD
		  ;	
			

			
