package rs.ac.bg.etf.pp1;

import java.util.Stack;
import rs.ac.bg.etf.pp1.ast.*;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

public class CodeGenerator extends VisitorAdaptor {
	
	private int mainPc;
	String err = "";
	private boolean hasMinus = false;
	private Stack<Integer> stackAdd;
    private Stack<Integer> stackMul;
    
    CodeGenerator() {
    	stackAdd = new Stack<>();
    	stackMul = new Stack<>();
    }
    
	public int getMainPc() {
		return mainPc;
	}
	
	public void visit(MethodDecl method) {
		Code.put(Code.exit);
		Code.put(Code.return_);
	}
	
	public void visit(MethodSignature method) {
		method.obj.setAdr(Code.pc);						
		if ("main".equalsIgnoreCase(method.getI2())) {
			mainPc = Code.pc;
	}
		Code.put(Code.enter);
		Code.put(method.obj.getLevel());
		Code.put(method.obj.getLocalSymbols().size());
	}
	
	public void visit(StatPrint statPrint) {
		if (statPrint.getExpr().obj.getType() == Tab.intType || statPrint.getExpr().obj.getType() == SymTabExtender.boolType){
			Code.loadConst(5);
			Code.put(Code.print);
		} else if (statPrint.getExpr().obj.getType() == Tab.charType) {
			Code.loadConst(1);
			Code.put(Code.bprint);
		}		
	}
	public void visit(StatPrintParam statPrint) {	
		if (statPrint.getExpr().obj.getType() == Tab.intType || statPrint.getExpr().obj.getType() == SymTabExtender.boolType){			
			Code.loadConst(statPrint.getN1());			//ovaj broj predstavlja broj razmaka od prethodnog znaka
			Code.put(Code.print);
		} else if (statPrint.getExpr().obj.getType() == Tab.charType) {
			Code.loadConst(statPrint.getN1());
			Code.put(Code.bprint);
		}		
	}
	
	public void visit(StatRead statRd) {
		Obj obj = statRd.getDesignator().obj;
		
		if (obj.getType().equals(Tab.charType)) {
			err = "USAO";
			Code.put(Code.bread);			//ako je char
		} else {
			
			Code.put(Code.read); 
		}
		Code.store(obj);
	}
	
	public void visit(ConstNum constNum) {		
		Code.load(constNum.obj);
	}
	
	public void visit(ConstBool constBool) {
		Code.load(constBool.obj);
	}
	
	public void visit(ConstChar constChar) {		
		Code.load(constChar.obj);
	}
	
	public void visit(EnumDeclDefNoVal enumDecl) {
		Code.load(enumDecl.obj);
	}
	
	public void visit(EnumDeclDefVal enumDecl) {
		Code.load(enumDecl.obj);
	}
	
	public void visit(DesignStatAssignment assign){
		Obj designator = assign.getDesignator().obj;		
		
		/*if (assign.getAssignement() instanceof AssignementExpr) {
			err = designator.getKind() + "" ;
			if (designator.getKind() == Obj.Elem) {
				Code.store(designator);
			}
			else {
				Code.store(designator);
			}
			
			//err = "design nakon store " + assign.getAssignement().toString();
		}*/	
		Code.store(designator);
		
	}
	
	public void visit(DesignStatInc design) {
		Obj designator = design.getDesignator().obj;
		
		if (designator.getKind() == Obj.Elem) {		//ako je niz
			Code.put(Code.dup2);
			Code.put(Code.aload);
			Code.put(Code.const_1);
			Code.put(Code.add);			
			Code.put(Code.astore);
		} else {
			Code.load(designator);
			Code.loadConst(1);
			Code.put(Code.add);
			Code.store(designator);
		}
	}
	
	public void visit(DesignStatDec design) {
		Obj designator = design.getDesignator().obj;
		
		if (designator.getKind() == Obj.Elem) {		//ako je niz
			Code.put(Code.dup2);
			Code.put(Code.aload);
			Code.put(Code.const_1);
			Code.put(Code.sub);			
			Code.put(Code.astore);
		} else {
			Code.load(designator);
			Code.loadConst(1);
			Code.put(Code.sub);
			Code.store(designator);
		}
	}
	
	public void visit(FactorDesign design) {
		Designator designator = design.getDesignator();
		
		/*if (designator instanceof DesIdent) {
			if (designator.obj.getType().getKind() == Struct.Array) {
				Code.load(designator.obj);
			}
			else {
				Code.load(design.obj);
			}
		}
		else if (designator instanceof DesEnum) {
			Code.load(designator.obj;
		else {			
			Code.load(design.obj);
		}*/
		Code.load(designator.obj);
		
	}
	
	
	public void visit(ArrayName arr) {	//stavi objekat niza na stek	
		Code.load(arr.obj);		
	}

	public void  visit(AddopExp addopExpr) {
		Code.put(stackAdd.pop());
	}

	public void visit(NegTermExpr negExpr) {
		Code.put(Code.neg);
	}
	
	
	public void visit(TermMulop mulop) {
		Code.put(stackMul.pop());
	}
	
	public void visit(FactorNewArr factor) {
		Code.put(Code.newarray);
		
		if (factor.obj.getType().getKind() == Struct.Array) {
			if (factor.obj.getType().getElemType().equals(Tab.intType) || factor.obj.getType().getElemType().equals(SymTabExtender.boolType)) {
				Code.put(1);				
			}
			else {
				Code.put(0);
			}
		}
		else {
			if (factor.obj.getType().getKind() == Tab.intType.getKind() || factor.obj.getType().getKind() == SymTabExtender.boolType.getKind()) {
				Code.put(1);
			}
			else {
				Code.put(0);
			}			
		}
	}
	
	public void visit(Addop_Plus addop) {		
		stackAdd.push(Code.add);			//cuvamo znak na nasem steku, da bismo ga kasnije u AddopExp skinuli i stavili na expr stek
	}

	public void visit(Addop_Minus addop) {		
		stackAdd.push(Code.sub);
	}
	
	public void visit(Mulop_Mul mulop) {
		stackMul.push(Code.mul);
	}
	
	public void visit(Mulop_Div mulop) {
		stackMul.push(Code.div);
	}
	
	public void visit(Mulop_Mod mulop) {
		stackMul.push(Code.rem);
	}
	
	
}
