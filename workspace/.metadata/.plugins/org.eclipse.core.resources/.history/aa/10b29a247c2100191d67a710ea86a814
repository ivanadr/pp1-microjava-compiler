package rs.ac.bg.etf.pp1;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import rs.ac.bg.etf.pp1.ast.*;
import rs.etf.pp1.symboltable.*;
import rs.etf.pp1.symboltable.concepts.*;


public class SemanticAnalyzer extends VisitorAdaptor {
	
	int printCallCount = 0;
	boolean errorDetected = false;
	boolean mainFound = false;
	private Obj currentTypeObj = Tab.noObj;	
	private Obj currentClass = Tab.noObj;
	private Obj currentMethod = Tab.noObj;
	
	private Struct currentType = Tab.noType;
	int enumVal = 0;
	
    int globalVarCnt = 0;
    int globalConstCnt = 0;
    int globalArrayCnt = 0;
    int mainLocalVarCnt = 0;
    int mainStatementCnt = 0;
    int nVars; 
    
    private Map<Integer, String> enumMap;

	Logger log = Logger.getLogger(getClass());
	
	public SemanticAnalyzer() {	
		enumMap = new HashMap<>();
	}
	
	
	public void report_error(String message, SyntaxNode info) {	
		errorDetected = true;
		StringBuilder msg = new StringBuilder(message);
		int line = (info == null) ? 0: info.getLine();
		if (line != 0)
			msg.append (" na liniji ").append(line);
		log.error(msg.toString());
	}

	public void report_info(String message, SyntaxNode info) {
		StringBuilder msg = new StringBuilder(message); 
		int line = (info == null) ? 0: info.getLine();
		if (line != 0)
			msg.append (" na liniji ").append(line);
		log.info(msg.toString());
	}
	
	
	 private void varInsert(String varName, Struct varType, int varLine) {
	        if (Tab.currentScope().findSymbol(varName) == null) {
	            int kind;
	            if (!currentClass.equals(Tab.noObj) && currentMethod.equals(Tab.noObj)) {
	                kind = Obj.Fld;
	            } else {
	                kind = Obj.Var;
	            }

	            Obj temp = Tab.insert(kind, varName, varType);

	            if (temp.getKind() == Obj.Var) {
	                if (temp.getType().getKind() == Struct.Array) {
	                    if (temp.getLevel() == 0) {
	                        globalArrayCnt++;
	                    }
	                } else {
	                    if (temp.getLevel() == 0) {
	                        globalVarCnt++;
	                    } else if ("main".equalsIgnoreCase(currentMethod.getName())) {
	                        mainLocalVarCnt++;
	                    }
	                }
	            }
	        } else {
	            report_error("Greska na " + varLine + "(" + varName + ") vec deklarisano", null);
	        }
	    }

	 public boolean passed(){
	    return !errorDetected;
	 }
	 
/********************************************VISIT****************************************************
******************************************************************************************************/
 
	 
	public void visit(ProgName progName) {
		progName.obj = Tab.insert(Obj.Prog, progName.getProgName(), Tab.noType);	//proName.obj je obj cvor na koji ukazuje atribut p iz gramatike i mi u njega stavljamo simbol koji smo ubacili 
		Tab.openScope();
	}
	
	public void visit(Program program) {
		Tab.chainLocalSymbols(program.getProgName().obj);
    	Tab.closeScope();
	}

	public void visit(ConstDecl constDecl) {
		
	}

	public void visit(ConstDef constDef) {
		if (constDef.getConstVal().obj.getType().equals(currentType)) {
			if (Tab.find(constDef.getConstName()) == Tab.noObj) {
				report_info("Deklarisana konstanta "  + "'" + constDef.getConstName() + "'", constDef);
				Obj obj = Tab.insert(Obj.Con, constDef.getConstName(), constDef.getConstVal().obj.getType());
				obj.setAdr(constDef.getConstVal().obj.getAdr());
				obj.setLevel(0);
			}
			else {
				report_error("Greska na " + constDef.getLine() + "(" + constDef.getConstName() + ") vec deklarisano", null);
			}
		} 
		else {
			report_error("Greska na " + constDef.getLine() + ", nekompatibilni tipovi", null);
		}
		 
	}

	public void visit(ConstNum constNum) {
        constNum.obj = new Obj(Obj.Con, "", Tab.intType, constNum.getNum(), Obj.NO_VALUE);	//ovde postavimo da ce obj cvor konstante imati u polju za adresu vrednost same konstante	
    }
	
	public void visit(ConstChar constChar) {
       constChar.obj = new Obj(Obj.Con, "", Tab.charType, constChar.getChr(), Obj.NO_VALUE);
    }
	
	public void visit(ConstBool constBool) {
       constBool.obj = new Obj(Obj.Con, "", SymTabExtender.boolType, Boolean.valueOf(constBool.getBool()) ? 1 : 0, Obj.NO_VALUE);
    }
	
	public void visit(GlobalVarDecl globalVarDecl) {
		currentType = Tab.noType;
	}
	
	public void visit(VarDeclDefineSingle varDecl) {
		//varInsert(varDecl.getName(), currentTypeObj.getType(), varDecl.getLine());
		
		if (Tab.currentScope().findSymbol(varDecl.getName()) == null) {		
			report_info("Deklarisana promenljiva "+ varDecl.getName() + " tipa " + currentType.getKind(), varDecl);
			Obj obj = Tab.insert(Obj.Var, varDecl.getName(), currentType);
		}
		else {
			report_error("Greska na liniji " + varDecl.getLine() + " :promenljiva " + varDecl.getName() + " je vec deklarisana", varDecl);
		}
		globalVarCnt++;
        
	}
	
	public void visit(VarDeclDefineArray varDecl) {
		//varInsert(varDeclDefineArray.getName(), currentTypeObj.getType(), varDeclDefineArray.getLine());
		if (Tab.currentScope().findSymbol(varDecl.getName()) == null) {		
			report_info("Deklarisana promenljiva niza "+ varDecl.getName() + " tipa " + currentType.getKind(), varDecl);
			
			//Obj obj = Tab.insert(Obj.Var, varDecl.getName(), currentType);
			Obj obj = Tab.insert(Obj.Var, varDecl.getName(), new Struct(Struct.Array, currentType));
		}
		else {
			report_info("Greska na liniji" + varDecl.getLine() + " :promenljiva " + varDecl.getName() + " je vec deklarisana", varDecl);
		}
		globalArrayCnt++;
	}
	
	public void visit(VarDecl varDecl) {      
        mainLocalVarCnt++;
    }
	
	public void visit(EnumDecl enumDecl) {
		//varInsert(enumDecl.getI1(), currentTypeObj.getType(), enumDecl.getLine()); 
		enumDecl.obj = Tab.insert(Obj.Type, enumDecl.getI1(), new Struct(Struct.Enum));
		enumMap.put(Struct.Enum, enumDecl.getI1());
		enumVal = 0; 
		report_info("Deklarisan novi enum tip " + enumDecl.getI1(), enumDecl);
	}
	
	public void visit(EnumDeclDefNoVal enumDecl) {
		//varInsert(enumDecl.getName(), currentTypeObj.getType(), enumDecl.getLine());
		/*if (Tab.currentScope().findSymbol(enumDecl.getName()) == null) {
			Obj obj = Tab.insert(Obj.Con, enumDecl.getName(), currentType);
			report_info("Deklarisan novi enum element " + enumDecl.getName() + ", kind mu je " + obj.getKind(), enumDecl);
		}*/
		enumDecl.obj = Tab.insert(Obj.Con, enumDecl.getName(), Tab.intType);
		enumDecl.obj.setAdr(enumVal++);
	}
	
	public void visit(EnumDeclDefVal enumDecl) {
		//varInsert(enumDecl.getName(), currentTypeObj.getType(), enumDecl.getLine());
		/*if (Tab.currentScope().findSymbol(enumDecl.getName()) == null) {
			Obj obj = Tab.insert(Obj.Con, enumDecl.getName(), currentType);
			report_info("Deklarisan novi enum element " + enumDecl.getName() + ", kind mu je " + obj.getKind(), enumDecl);
		}*/
		enumDecl.obj = Tab.insert(Obj.Con, enumDecl.getName(), Tab.intType);
		enumDecl.obj.setAdr(enumDecl.getNum());
		enumVal = enumDecl.getNum() + 1;
	}
	
	public void visit(Type type){
    	Obj typeNode = Tab.find(type.getTypeName());
    	if(typeNode == Tab.noObj && (typeNode.getType()!=SymTabExtender.boolType)){						//ako nije nadjen, vratice se noObj
    		report_error("Nije pronadjen tip " + type.getTypeName() + " u tabeli simbola! ", null);
    		type.struct = Tab.noType;
    	}else{
    		if(Obj.Type == typeNode.getKind()){			//proveravamo da li je typeNode koji je vracen po tipu Type 
    			type.struct = typeNode.getType();
    		}else{				//ako nije, znaci da je vracen obj cvor koji nije type nego ko zna sta, to je greska
    			report_error("Greska: Ime " + type.getTypeName() + " ne predstavlja tip!", type);
    			type.struct = Tab.noType;
    		}
    	}
    	currentType = typeNode.getType();
    }
	
	public void visit(VoidableType voidableType) {
		voidableType.obj = Tab.noObj;
	}
	
	public void visit(MethodDecl methodDecl) {
		Tab.chainLocalSymbols(currentMethod);
		Tab.closeScope();
		currentMethod = Tab.noObj;
	}	
	
	public void visit(MethodSignature methodSignature) {
		currentMethod = Tab.noObj;
		if (Tab.currentScope().findSymbol(methodSignature.getI2()) == null) {
			currentMethod = Tab.insert(Obj.Meth, methodSignature.getI2(), methodSignature.getVoidableType().obj.getType());
			methodSignature.obj = currentMethod;
			
			if (currentClass.equals(Tab.noObj) && "main".equalsIgnoreCase(currentMethod.getName())) {
                mainFound = true;

                if (!currentMethod.getType().equals(Tab.noType)) {
                    report_error("Greska na " + methodSignature.getLine() + ": return vrednost metode main mora biti void", null);
                }
            }
			Tab.openScope();
			report_info("Obradjuje se funkcija " + methodSignature.getI2() + " na liniji " + methodSignature.getLine(), null);
		}
	}

	public void visit(DesignStatAssignment assign) {
		Obj desObj = assign.getDesignator().obj;
		int kind = desObj.getKind();
		
		report_info("USAO " + desObj.getName(), null);
		
		if (kind != Obj.Var && kind != Obj.Elem && kind != Obj.Fld) {
			report_error("Greska na " + assign.getLine() + ": neispravna leva strana dodele", null);
		}	
		else if (kind == Obj.Elem) {			//ako je leva strana element niza
			report_info("USAO " + desObj.getType().getKind(), null);
			if (desObj.getType().getKind()==1 && assign.getAssignement().struct.getKind()==6) {
				report_info("Dodeljenja vrednost enum i int", null);
			}
			else if(desObj.getType().getKind()==6 && assign.getAssignement().struct.getKind()==1) {
				report_info("Indeks je enum, dodela je int", null);
			}
			else if (!assign.getAssignement().struct.assignableTo(desObj.getType())) {			
				 report_error(" OVDE Greska: nekompatibilni tipovi u dodeli", assign);
				 report_error(desObj.getType().getKind() + " je kind designatora, " + assign.getAssignement().struct.getKind() + " je kind assignmenta", null);		 
			} 
		} 
		else if (assign.getAssignement().struct.getKind() == 3) {		//ako je desna strana element niza
			if (kind == Obj.Elem) {		//ako je i leva strana element niza
				if (!assign.getAssignement().struct.getElemType().assignableTo(desObj.getType().getElemType())) {
					report_error("Greska: nekompatibilni tipovi pri dodeli elementa niza drugom elementu niza", assign);
				}
			} 
			else if (desObj.getType().getKind() == 3) {		//ako je levo promenljiva niza, ne element
				if (!assign.getAssignement().struct.getElemType().assignableTo(desObj.getType().getElemType())){
					report_error("Greska: nekompatabilan tip pri kreiranju novog niza", assign);				
				}
			}
			/*else if (!assign.getAssignement().struct.getElemType().assignableTo(desObj.getType())) {	//ako levo nije niz, nego obicna promenljiva
				report_error("Greska: nekompatibilni tipovi pri dodeli elementa niza promenljivoj", assign);
			}*/
		}
		else if (desObj.getType().getKind()==6 && assign.getAssignement().struct.getKind()==1) {
			report_info("Dodeljenja vrednost enuma promenljivoj enuma", null);
		}
		else if (!assign.getAssignement().struct.assignableTo(desObj.getType())) {		//za dodele promenljivih, bez nizova		
			 report_error("Greska: nekompatibilni tipovi u dodeli", assign);
			 report_error(desObj.getType().getKind() + " je kind designatora, " + assign.getAssignement().struct.getKind() + " je kind assignmenta", null);		 
		}
		else {
			report_info("Dodeljenja vrednost: " + desObj.getType().getKind() + " je design," + assign.getAssignement().struct.getKind() + " je assign", null);
		}
		
	}
	
	public void visit(AssignementExpr assign) {
		assign.struct = assign.getExpr().struct;
	}
	
	public void visit(AssignementExprError assign) {
		assign.struct = Tab.noType;
	}
	
	
	public void visit(DesignStatInc design) {
		Obj designObj = design.getDesignator().obj;
        int kind = designObj.getKind();
        
        if (kind != Obj.Var && kind != Obj.Elem && kind != Obj.Fld) {
        	report_error("Greska: " + designObj.getName() + " nije promenljiva",  design);
        }
        else if(!designObj.getType().equals(Tab.intType)) {
        	report_error("Greska: " + designObj.getName() + " nije int", design);
        }
        else {
        	report_info("Inkrementirana vrednost", null);
        }
	}
	
	public void visit(DesignStatDec design) {
		Obj designObj = design.getDesignator().obj;
        int kind = designObj.getKind();
        
        if (kind != Obj.Var && kind != Obj.Elem && kind != Obj.Fld) {
        	report_error("Greska: " + designObj.getName() + " nije promenljiva", design);
        }
        else if(!designObj.getType().equals(Tab.intType)) {
        	report_error("Greska: " + designObj.getName() + " nije int", design);
        }
        else {
        	report_info("Dekrementirana vrednost", null);
        }
	}
	
	public void visit(StatRead statRead) {
		Obj designObj = statRead.getDesignator().obj;
		int kind = designObj.getKind();
		
		if (kind != Obj.Var && kind != Obj.Elem && kind != Obj.Fld) {
            report_error("Greska: parametar read funkcije nije promenljiva", statRead);
        } else if (!designObj.getType().equals(Tab.intType) && !designObj.getType().equals(Tab.charType) && !designObj.getType().equals(SymTabExtender.boolType)) {
            report_error("Greska: parametar read funkcije je nekorektnog tipa", statRead);
        }
	}
	
	public void visit(StatPrint statPrint) {
		Struct parType = statPrint.getExpr().struct;
		
		if (parType.getKind() == Struct.Array) {
			if (!parType.getElemType().equals(Tab.intType) && !parType.getElemType().equals(Tab.charType) && !parType.getElemType().equals(SymTabExtender.boolType)) {
	            report_error("Greska: parametar print funkcije nije validan", statPrint);
	        }
		}		
		else if (!parType.equals(Tab.intType) && !parType.equals(Tab.charType) && !parType.equals(SymTabExtender.boolType)) {
            report_error("Greska: parametar print funkcije nije validan", statPrint);
        }
		else {
			printCallCount++;
		}
	}

	public void visit(StatPrintParam statPrint) {
		Struct parType = statPrint.getExpr().struct;
		
		if (!parType.equals(Tab.intType) && !parType.equals(Tab.charType) && !parType.equals(SymTabExtender.boolType)) {
            report_error("Greska: parametar print funkcije nije validan", statPrint);
        } 
		else {
			printCallCount++;
		}
	}
	
	public void  visit(AddopExp addopExpr) {		
		//if(!addopExpr.getExpr().struct.compatibleWith(addopExpr.getTerm().struct)) {
		if (!addopExpr.getExpr().struct.equals(Tab.intType) && addopExpr.getExpr().struct.getKind()!=6 && !addopExpr.getTerm().equals(Tab.intType) && addopExpr.getTerm().struct.getKind()!=6) {	
			report_error(addopExpr.getExpr().struct.getKind() + "je kind expr, " + addopExpr.getTerm().struct.getKind() + " je kind terma" , null);
			report_error("Greska: clanovi izraza nisu korektnog tipa" , addopExpr);
		}
		if (addopExpr.getExpr().struct.getKind() == 1) {		//ako je levi operand int, proslediti njegov tip
			addopExpr.struct = addopExpr.getExpr().struct;
		}
		else {
			addopExpr.struct = addopExpr.getTerm().struct;		//ako levi operand nije int, onda je enum, proslediti tip desnog koji je int
		}
		
	}
	
	public void visit(TermExpr termExpr) {
		termExpr.struct = termExpr.getTerm().struct;
	}
	
	public void  visit(NegTermExpr negExpr) {
		if (!negExpr.getTerm().equals(Tab.intType) && negExpr.getTerm().struct.getKind()!=6){
			report_error("Greska: negira se clan koji nije int" , negExpr);
		}
		negExpr.struct = negExpr.getTerm().struct;
	}
	
	public void visit(TermFact termFact) {
		termFact.struct = termFact.getFactor().struct;
	}
	
	public void visit(TermMulop termMulop) {
		
		if (termMulop.getTerm().struct.getKind() == 3){
			if (termMulop.getTerm().struct.getElemType().getKind()!=1) {
				report_error("Greska: clanovi izraza nisu korektnog tipa", termMulop);
			}
		} else if (termMulop.getFactor().struct.getKind() == 3) {
			if (termMulop.getFactor().struct.getElemType().getKind()!=1) {
				report_error("Greska: clanovi izraza nisu korektnog tipa", termMulop);
			}
		}else if (!termMulop.getTerm().struct.equals(Tab.intType) && termMulop.getTerm().struct.getKind()!=6 && !termMulop.getFactor().struct.equals(Tab.intType) && termMulop.getFactor().struct.getKind()!=6 ) {
            report_error("Greska: clanovi izraza nisu korektnog tipa", termMulop);
        }
		
		if (termMulop.getTerm().struct.getKind() == 1) {		//ako je levi operand int, proslediti njegov tip
			termMulop.struct = termMulop.getTerm().struct;
		}
		else {
			termMulop.struct = termMulop.getFactor().struct;		//ako levi operand nije int, onda je enum, proslediti tip desnog koji je int
		}	
	}
	
	public void visit(FactorExpr factorExpr) {
		factorExpr.struct = factorExpr.getExpr().struct;
	}
	
	public void visit(FactorConst factorConst) {
		factorConst.struct = factorConst.getConstVal().obj.getType();
	}
	
	public void visit(FactorNewArr factorNewArr) {
		if (!factorNewArr.getExpr().struct.equals(Tab.intType)) {
			report_error("Greska: izraz unutar zagrada nije int", factorNewArr);
		}
		
		//factorNewArr.struct = factorNewArr.getExpr().struct; - ovo bi na tip stavilo tip indeksa, a ne tip niza
		//factorNewArr.struct = currentType;		
		factorNewArr.struct = new Struct(Struct.Array, currentType);		
	}
	
	
	 public void visit(FactorDesign factorDesign) {
		/*Obj designObj = factorDesign.getDesignator().obj;			NEPOTREBNE PROVERE
		int kind = designObj.getKind();		
		if (kind!=Obj.Con && kind != Obj.Var && kind != Obj.Elem && kind!=Obj.Type) {
			report_error(kind  + " je kind", null);
			report_error("Greska: " + designObj.getName() + " nije promenljiva", factorDesign);
		}*/
		 
		factorDesign.struct = factorDesign.getDesignator().obj.getType();		
	}
	
	/*public void visit(DesIdent desIdent) {	
		desIdent.obj = Tab.find(desIdent.getId());
		if (desIdent.obj.equals(Tab.noObj)) {
			report_error("Greska: " + desIdent.getId() + " nije pronadjen", desIdent);
			desIdent.obj = new Obj(Tab.noObj.getKind(), desIdent.getId(), Tab.noType);
		}
		else if (desIdent.getDes() instanceof DesArr) {
			desIdent.obj = new Obj(Obj.Elem, "elem", desIdent.obj.getType());
		}
		//report_info("Designator ident je: " + desIdent.obj.getName() + ", " + desIdent.obj.getType().getKind(), null);
	}*/
	 
	public void visit(DesIdent desIdent) {
		desIdent.obj = Tab.find(desIdent.getId());
		if (desIdent.obj.equals(Tab.noObj)) {
			report_error("Greska: " + desIdent.getId() + " nije pronadjen", desIdent);
			//desIdent.obj = new Obj(Tab.noObj.getKind(), desIdent.getId(), Tab.noType);
		}
	}
	
	public void visit(DesEnum desEnum) {
		desEnum.obj = Tab.find(desEnum.getIded());
	}
	
	public void visit(DesArr desArr) {	
		
		report_info("elem niza je " + desArr.getArrayName().obj.getName(), desArr);
		/*if (!desArr.getExpr().struct.equals(Tab.intType) && desArr.getExpr().struct.getKind() !=6) {			
			report_error("Greska: indeks u zagradama nije int", desArr);
		}	*/
		//desArr.obj = new Obj(Obj.Elem, "elem", desArr.obj.getType().getElemType()); - ovo je bilo pre zakoment
		if (desArr.getArrayName().obj.getType().getKind() != Struct.Array) {
			report_error("Greska: promenljiva " + desArr.obj.getName() + " nije niz", desArr);
		}
		else if (!desArr.getExpr().struct.equals(Tab.intType) && desArr.getExpr().struct.getKind()!=6) {
			report_error("Greska: indeks u zagradama nije int", desArr);
		}
		
		desArr.obj = new Obj(Obj.Elem, desArr.getArrayName().obj.getName(), desArr.getArrayName().obj.getType().getElemType());		//uzimamo tip elemenata niza, to smo deklarisali kad smo stavili niz u tabelu simbola pri deklaraciji
		
	}
	
	public void visit(ArrayName arrName) {
		arrName.obj = Tab.find(arrName.getId());
		if (arrName.obj.equals(Tab.noObj)) {
			report_error("Greska: " + arrName.getId() + " nije pronadjen", arrName);
		}
		report_info("ime niza je " + arrName.obj.getName(), arrName);
	}
	
	
	
}

