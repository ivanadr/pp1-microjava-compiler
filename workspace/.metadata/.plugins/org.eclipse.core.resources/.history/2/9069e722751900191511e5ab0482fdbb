package rs.ac.bg.etf.pp1;

import org.apache.log4j.Logger;

import rs.ac.bg.etf.pp1.ast.*;
import rs.etf.pp1.symboltable.*;
import rs.etf.pp1.symboltable.concepts.*;


public class SemanticAnalyzer extends VisitorAdaptor {
	
	int printCallCount = 0;
	boolean errorDetected = false;
	boolean mainFound = false;
	private Obj currentTypeObj = Tab.noObj;	
	private Obj currentClass = Tab.noObj;
	private Obj currentMethod = Tab.noObj;
	
	private Struct currentType = Tab.noType;

    int globalVarCnt = 0;
    int globalConstCnt = 0;
    int globalArrayCnt = 0;
    int mainLocalVarCnt = 0;
    int mainStatementCnt = 0;

	Logger log = Logger.getLogger(getClass());
	
	public SemanticAnalyzer() {	
	}
	
	
	public void report_error(String message, SyntaxNode info) {	
		errorDetected = true;
		StringBuilder msg = new StringBuilder(message);
		int line = (info == null) ? 0: info.getLine();
		if (line != 0)
			msg.append (" na liniji ").append(line);
		log.error(msg.toString());
	}

	public void report_info(String message, SyntaxNode info) {
		StringBuilder msg = new StringBuilder(message); 
		int line = (info == null) ? 0: info.getLine();
		if (line != 0)
			msg.append (" na liniji ").append(line);
		log.info(msg.toString());
	}
	
	
	 private void varInsert(String varName, Struct varType, int varLine) {
	        if (Tab.currentScope().findSymbol(varName) == null) {
	            int kind;
	            if (!currentClass.equals(Tab.noObj) && currentMethod.equals(Tab.noObj)) {
	                kind = Obj.Fld;
	            } else {
	                kind = Obj.Var;
	            }

	            Obj temp = Tab.insert(kind, varName, varType);

	            if (temp.getKind() == Obj.Var) {
	                if (temp.getType().getKind() == Struct.Array) {
	                    if (temp.getLevel() == 0) {
	                        globalArrayCnt++;
	                    }
	                } else {
	                    if (temp.getLevel() == 0) {
	                        globalVarCnt++;
	                    } else if ("main".equalsIgnoreCase(currentMethod.getName())) {
	                        mainLocalVarCnt++;
	                    }
	                }
	            }
	        } else {
	            report_error("Greska na " + varLine + "(" + varName + ") vec deklarisano", null);
	        }
	    }

	 public boolean passed(){
	    return !errorDetected;
	 }
	 
/********************************************VISIT****************************************************
******************************************************************************************************/
 
	 
	public void visit(ProgName progName) {
		progName.obj = Tab.insert(Obj.Prog, progName.getProgName(), Tab.noType);	//proName.obj je obj cvor na koji ukazuje atribut p iz gramatike i mi u njega stavljamo simbol koji smo ubacili 
		Tab.openScope();
	}
	
	public void visit(Program program) {
		Tab.chainLocalSymbols(program.getProgName().obj);
    	Tab.closeScope();
	}

	public void visit(ConstDecl constDecl) {
		
	}

	public void visit(ConstDef constDef) {
		if (constDef.getConstVal().obj.getType().equals(currentType)) {
			if (Tab.find(constDef.getConstName()) == Tab.noObj) {
				report_info("Deklarisana konstanta "  + "'" + constDef.getConstName() + "'", constDef);
				Obj obj = Tab.insert(Obj.Con, constDef.getConstName(), constDef.getConstVal().obj.getType());
				obj.setAdr(constDef.getConstVal().obj.getAdr());
			}
			else {
				report_error("Greska na " + constDef.getLine() + "(" + constDef.getConstName() + ") vec deklarisano", null);
			}
		} 
		else {
			report_error("Greska na " + constDef.getLine() + ", nekompatibilni tipovi", null);
		}
		 
	}

	public void visit(ConstNum constNum) {
        constNum.obj = new Obj(Obj.Con, "", Tab.intType, constNum.getNum(), Obj.NO_VALUE);	
    }
	
	public void visit(ConstChar constChar) {
       constChar.obj = new Obj(Obj.Con, "", Tab.charType, constChar.getChr(), Obj.NO_VALUE);
    }
	
	public void visit(ConstBool constBool) {
       // constBool.obj = new Obj(Obj.Con, "", SymTabExtender.boolType, Boolean.valueOf(constBool.getBool()) ? 1 : 0, Obj.NO_VALUE);
		//constBool.struct = Tab.boolType;
    }
	
	public void visit(GlobalVarDecl globalVarDecl) {
		currentType = Tab.noType;
	}
	
	public void visit(VarDeclDefineSingle varDecl) {
		//varInsert(varDecl.getName(), currentTypeObj.getType(), varDecl.getLine());
		
		if (Tab.currentScope().findSymbol(varDecl.getName()) == null) {		
			report_info("Deklarisana promenljiva "+ varDecl.getName() + " tipa " + currentType.getKind(), varDecl);
			Obj obj = Tab.insert(Obj.Var, varDecl.getName(), currentType);
		}
		else {
			report_info("Greska na liniji" + varDecl.getLine() + " :promenljiva " + varDecl.getName() + " je vec deklarisana", varDecl);
		}
		globalVarCnt++;
        
	}
	
	public void visit(VarDeclDefineArray varDecl) {
		//varInsert(varDeclDefineArray.getName(), currentTypeObj.getType(), varDeclDefineArray.getLine());
		if (Tab.currentScope().findSymbol(varDecl.getName()) == null) {		
			report_info("Deklarisana promenljiva niza "+ varDecl.getName() + " tipa " + currentType.getKind(), varDecl);
			Obj obj = Tab.insert(Obj.Var, varDecl.getName(), currentType);
		}
		else {
			report_info("Greska na liniji" + varDecl.getLine() + " :promenljiva " + varDecl.getName() + " je vec deklarisana", varDecl);
		}
		globalArrayCnt++;
		//report_info("Deklarisana promenljiva niza "+ varDecl.getName(), varDecl);
	}
	
	public void visit(VarDecl varDecl) {        
		
        mainLocalVarCnt++;
    }
	
	public void visit(EnumDecl enumDecl) {
		//varInsert(enumDecl.getI1(), currentTypeObj.getType(), enumDecl.getLine()); 
		Obj enumObj = Tab.insert(Obj.Type, enumDecl.getI1(), new Struct(Struct.Enum));
		report_info("Deklarisan novi enum tip " + enumDecl.getI1() + ", njegov kind je " + enumDecl., enumDecl);
	}
	
	public void visit(EnumDeclDefNoVal enumDecl) {
		//varInsert(enumDeclDefNoVal.getName(), currentTypeObj.getType(), enumDeclDefNoVal.getLine());
		if (Tab.currentScope().findSymbol(enumDecl.getName()) == null) {
			Obj obj = Tab.insert(Obj.Con, enumDecl.getName(), currentType);
			report_info("Deklarisan novi enum element " + enumDecl.getName() + ", kind mu je " + obj.getKind(), enumDecl);
		}
	}
	
	public void visit(EnumDeclDefVal enumDecl) {
		if (Tab.currentScope().findSymbol(enumDecl.getName()) == null) {
			Obj obj = Tab.insert(Obj.Con, enumDecl.getName(), currentType);
			report_info("Deklarisan novi enum element " + enumDecl.getName() + ", kind mu je " + obj.getKind(), enumDecl);
		}
		
	}
	
	public void visit(Type type){
    	Obj typeNode = Tab.find(type.getTypeName());
    	if(typeNode == Tab.noObj && (typeNode.getType()!=SymTabExtender.boolType)){						//ako nije nadjen, vratice se noObj
    		report_error("Nije pronadjen tip " + type.getTypeName() + " u tabeli simbola! ", null);
    		type.struct = Tab.noType;
    	}else{
    		if(Obj.Type == typeNode.getKind()){			//proveravamo da li je typeNode koji je vracen po tipu Type 
    			type.struct = typeNode.getType();
    		}else{				//ako nije, znaci da je vracen obj cvor koji nije type nego ko zna sta, to je greska
    			report_error("Greska: Ime " + type.getTypeName() + " ne predstavlja tip!", type);
    			type.struct = Tab.noType;
    		}
    	}
    	currentType = typeNode.getType();
    }
	
	public void visit(VoidableType voidableType) {
		voidableType.obj = Tab.noObj;
	}
	public void visit(MethodSignature methodSignature) {
		currentMethod = Tab.noObj;
		if (Tab.currentScope().findSymbol(methodSignature.getI2()) == null) {
			currentMethod = Tab.insert(Obj.Meth, methodSignature.getI2(), methodSignature.getVoidableType().obj.getType());
			
			if (currentClass.equals(Tab.noObj) && "main".equalsIgnoreCase(currentMethod.getName())) {
                mainFound = true;

                if (!currentMethod.getType().equals(Tab.noType)) {
                    report_error("Greska na " + methodSignature.getLine() + ": return vrednost metode main mora biti void", null);
                }
            }
			Tab.openScope();
			report_info("Obradjuje se funkcija " + methodSignature.getI2() + " na liniji " + methodSignature.getLine(), null);
		}
	}
	
	
	
	public void visit(DesignStatAssignment assign) {
		Obj desObj = assign.getDesignator().obj;
		int kind = desObj.getKind();
		
		if (kind != Obj.Var && kind != Obj.Elem && kind != Obj.Fld) {
			report_error("Greska na " + assign.getLine() + ": neispravna leva strana dodele", null);
		}	
		else if (assign.getAssignement().struct.getKind()==6 && kind==1){
			report_info("Dodeljenja vrednost enum i int", null);
		}
		else if (!assign.getAssignement().struct.assignableTo(desObj.getType())) {			
			 //report_error("Greska na " + assign.getLine() + ": nekompatibilni tipovi u dodeli", null);
			 report_error(desObj.getKind() + " je kind designatora, " + assign.getAssignement().struct.getKind() + " je kind assignmenta", null);
			 report_error(desObj.getName() + " je designator, " + assign.getAssignement().struct.getMembers() + " je  assignmenta", null);		 
		}
		else {
			report_info("Dodeljenja vrednost " + kind + " je design," + assign.getAssignement().struct.getKind() + " je assign", null);
		}		
	}
	
	public void visit(AssignementExpr assign) {
		assign.struct = assign.getExpr().struct;
	}
	
	public void visit(AssignementExprError assign) {
		assign.struct = Tab.noType;
	}
	
	
	public void visit(DesignStatInc design) {
		Obj designObj = design.getDesignator().obj;
        int kind = designObj.getKind();
        
        if (kind != Obj.Var && kind != Obj.Elem && kind != Obj.Fld) {
        	report_error("Greska na " + design.getLine() + ": " + designObj.getName() + " nije promenljiva", null);
        }
        else if(!designObj.getType().equals(Tab.intType)) {
        	report_error("Greska na " + design.getLine() + ": " + designObj.getName() + " nije int", null);
        }
        else {
        	report_info("Inkrementirana vrednost", null);
        }
	}
	
	public void visit(DesignStatDec design) {
		Obj designObj = design.getDesignator().obj;
        int kind = designObj.getKind();
        
        if (kind != Obj.Var && kind != Obj.Elem && kind != Obj.Fld) {
        	report_error("Greska na " + design.getLine() + ": " + designObj.getName() + " nije promenljiva", null);
        }
        else if(!designObj.getType().equals(Tab.intType)) {
        	report_error("Greska na " + design.getLine() + ": " + designObj.getName() + " nije int", null);
        }
        else {
        	report_info("Dekrementirana vrednost", null);
        }
	}
	
	public void visit(StatRead statRead) {
		Obj designObj = statRead.getDesignator().obj;
		int kind = designObj.getKind();
		
		if (kind != Obj.Var && kind != Obj.Elem && kind != Obj.Fld) {
            report_error("Greska na " + statRead.getLine() + ": parametar read funkcije nije promenljiva", null);
        } else if (!designObj.getType().equals(Tab.intType) && !designObj.getType().equals(Tab.charType) && !designObj.getType().equals(SymTabExtender.boolType)) {
            report_error("Greska na " + statRead.getLine() + ": parametar read funkcije je nekorektnog tipa", null);
        }
	}
	
	public void visit(StatPrint statPrint) {
		Struct parType = statPrint.getExpr().struct;
		
		if (!parType.equals(Tab.intType) && !parType.equals(Tab.charType) && !parType.equals(SymTabExtender.boolType)) {
            report_error("Greska na " + statPrint.getLine() + ": parametar print funkcije nije validan", null);
        }
		else {
			printCallCount++;
		}
	}

	public void visit(StatPrintParam statPrint) {
		Struct parType = statPrint.getExpr().struct;
		
		if (!parType.equals(Tab.intType) && !parType.equals(Tab.charType) && !parType.equals(SymTabExtender.boolType)) {
            report_error("Greska na " + statPrint.getLine() + ": parametar print funkcije nije validan", null);
        } 
		else {
			printCallCount++;
		}
	}
	
	public void  visit(AddopExp addopExpr) {
		if (!addopExpr.getExpr().struct.equals(Tab.intType) && !addopExpr.getTerm().equals(Tab.intType)){
			report_error("Greska na " + addopExpr.getLine() + ": clanovi izraza nisu int" , null);
		}
		//if(!addopExpr.getExpr().struct.compatibleWith(addopExpr.getTerm().struct)) {
		if (!addopExpr.getExpr().struct.equals(Tab.intType) && addopExpr.getExpr().struct.getKind()!=6 && !addopExpr.getTerm().equals(Tab.intType) && addopExpr.getTerm().struct.getKind()!=6) {	
			report_error(addopExpr.getExpr().struct.getKind() + "je kind expr, " + addopExpr.getTerm().struct.getKind() + " je kind terma" , null);
			report_error("Greska na " + addopExpr.getLine() + ": clanovi izraza nisu kompatibilni" , null);
		}
		
		addopExpr.struct = addopExpr.getExpr().struct;
	}
	
	public void visit(TermExpr termExpr) {
		termExpr.struct = termExpr.getTerm().struct;
	}
	
	public void  visit(NegTermExpr negExpr) {
		if (!negExpr.getTerm().equals(Tab.intType)){
			report_error("Greska na " + negExpr.getLine() + ": negira se clan koji nije int" , null);
		}
		negExpr.struct = negExpr.getTerm().struct;
	}
	
	public void visit(TermFact termFact) {
		termFact.struct = termFact.getFactor().struct;
	}
	
	public void visit(TermMulop termMulop) {
		
		if (!termMulop.getTerm().struct.equals(Tab.intType) || !termMulop.getFactor().struct.equals(Tab.intType)) {
            report_error("Greska na " + termMulop.getLine() + ": clanovi izraza nisu int", null);
        }
		termMulop.struct = termMulop.getFactor().struct;
	}
	
	public void visit(FactorExpr factorExpr) {
		factorExpr.struct = factorExpr.getExpr().struct;
	}
	
	public void visit(FactorConst factorConst) {
		factorConst.struct = factorConst.getConstVal().obj.getType();
	}
	
	public void visit(FactorNewArr factorNewArr) {
		if (!factorNewArr.getExpr().struct.equals(Tab.intType)) {
			report_error("Greska na " + factorNewArr.getLine() + ": izraz unutar zagrada nije int", null);
		}
		
		factorNewArr.struct = factorNewArr.getExpr().struct;
		//factorNewArr.struct = currentType;		
	}
	
	/*public void visit(FactorNew factorNew) {
		factorNew.struct = factorNew.getType().obj
	}*/
	
	 public void visit(FactorDesign factorDesign) {
		Obj designObj = factorDesign.getDesignator().obj;
		int kind = designObj.getKind();
		
		if (kind!=Obj.Con && kind != Obj.Var && kind != Obj.Elem && kind!=Obj.Type) {
			report_error(kind  + " je kind", null);
			report_error("Greska na " + factorDesign.getLine() + ": " + designObj.getName() + " nije promenljiva", null);
		}
		factorDesign.struct = factorDesign.getDesignator().obj.getType();		
	}
	
	public void visit(Designator desIdent) {
		desIdent.obj = Tab.find(desIdent.getId());
		
		if (desIdent.obj.equals(Tab.noObj)) {
			report_error("Greska na " + desIdent.getLine() + ": " + desIdent.getId() + " nije pronadjen", null);
			desIdent.obj = new Obj(Tab.noObj.getKind(), desIdent.getId(), Tab.noType);
		}
		report_info("Designator ident je: " + desIdent.obj.getName() + ", " + desIdent.obj.getKind(), null);
	}
	
	public void visit(DesEnum desEnum) {
		
	}
	
	public void visit(DesArr desArr) {
		
		if (!desArr.getExpr().struct.equals(Tab.intType) && desArr.getExpr().struct.getKind() !=6) {
			//report_error(desArr.getExpr().struct.getKind() + " je kind u indeksu", null);
			report_error("Greska na " + desArr.getLine() + ": indeks u zagradama nije int", null);
		}		
	}
	
	
	
}
