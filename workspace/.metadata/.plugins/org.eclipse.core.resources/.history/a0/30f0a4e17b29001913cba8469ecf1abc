package rs.ac.bg.etf.pp1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import rs.ac.bg.etf.pp1.ast.*;
import rs.etf.pp1.symboltable.*;
import rs.etf.pp1.symboltable.concepts.*;


public class SemanticAnalyzer extends VisitorAdaptor {
	
	int printCallCount = 0;
	boolean errorDetected = false;
	boolean mainFound = false;
	private Obj currentTypeObj = Tab.noObj;	
	private Obj currentClass = Tab.noObj;
	private Obj currentMethod = Tab.noObj;
	private Obj currentEnum = Tab.noObj;
	private Struct currentType = Tab.noType;
	private String curType = "";
	int enumVal = 0;
	private boolean forIndex = false;
	int readCallCount = 0;
    int globalVarCnt = 0;
    int globalConstCnt = 0;
    int globalArrayCnt = 0;
    int mainLocalVarCnt = 0;
    int mainStatementCnt = 0;
    int nVars; 
    
    private Map<String, Obj> enumMap;				//za cuvanje enuma i njegovih konstanti: String cuva Broj.JEDAN, Obj cuva Broj
    public Map<String, String> localEnumsMap;		//za cuvanje lokalnih enumskih promenljivih
    
	Logger log = Logger.getLogger(getClass());
	ArrayList<String> greske;
	
	public SemanticAnalyzer() {	
		enumMap = new HashMap<>();
		localEnumsMap = new HashMap<>();
		greske = new ArrayList<>();		
	}
	
	public ArrayList<String> getGreske() {
		return greske;
	}
	
	public void report_error(String message, SyntaxNode info) {	
		errorDetected = true;
		StringBuilder msg = new StringBuilder(message);
		int line = (info == null) ? 0: info.getLine();
		if (line != 0)
			msg.append (" na liniji ").append(line);
		log.error(msg.toString());
		greske.add(msg.toString());		
	}

	public void report_info(String message, SyntaxNode info) {
		StringBuilder msg = new StringBuilder(message); 
		int line = (info == null) ? 0: info.getLine();
		if (line != 0)
			msg.append (" na liniji ").append(line);
		log.info(msg.toString());
	}
	
	private boolean checkEnums(Obj left, Obj right) {
		boolean same = false;
		//report_info("levo " + left.getName() + " desno " + right.getName(),null);
		String lenum = localEnumsMap.get(left.getName());	//dobijam tip enuma leve strane		
		Obj rEnum = enumMap.get(right.getName());			//dobijam tip enuma sa desne strane
		if (rEnum!=null && !lenum.equals(null)) {
			if (lenum.equals(rEnum.getName())) {
				same = true;
			}
			else {
				same = false;
			}
		}
		return same;
	}
	
	 private void varInsert(String varName, Struct varType, int varLine) {
	        if (Tab.currentScope().findSymbol(varName) == null) {
	            int kind;
	            if (!currentClass.equals(Tab.noObj) && currentMethod.equals(Tab.noObj)) {
	                kind = Obj.Fld;
	            } else {
	                kind = Obj.Var;
	            }

	            Obj temp = Tab.insert(kind, varName, varType);

	            if (temp.getKind() == Obj.Var) {
	                if (temp.getType().getKind() == Struct.Array) {
	                    if (temp.getLevel() == 0) {
	                        globalArrayCnt++;
	                    }
	                } else {
	                    if (temp.getLevel() == 0) {
	                        globalVarCnt++;
	                    } else if ("main".equalsIgnoreCase(currentMethod.getName())) {
	                        mainLocalVarCnt++;
	                    }
	                }
	            }
	        } else {
	            report_error("Greska na " + varLine + "(" + varName + ") vec deklarisano", null);
	        }
	    }

	 public boolean passed(){
	    return !errorDetected;
	 }
	 
/********************************************VISIT****************************************************
******************************************************************************************************/
 
	 
	public void visit(ProgName progName) {
		progName.obj = Tab.insert(Obj.Prog, progName.getProgName(), Tab.noType);	//proName.obj je obj cvor na koji ukazuje atribut p iz gramatike i mi u njega stavljamo simbol koji smo ubacili 
		Tab.openScope();
	}
	
	public void visit(Program program) {
		//program.obj.setLocals(Tab.currentScope.getLocals());
		Tab.chainLocalSymbols(program.getProgName().obj);
		nVars = Tab.currentScope.getnVars();
    	Tab.closeScope();
	}

	public void visit(ConstDecl constDecl) {
		
	}

	public void visit(ConstDef constDef) {
		if (constDef.getConstVal().obj.getType().equals(currentType)) {
			if (Tab.find(constDef.getConstName()) == Tab.noObj) {		//proveravam da li vec postoji const sa tim imenom
				report_info("Deklarisana konstanta "  + "'" + constDef.getConstName() + "'", constDef);
				Obj obj = Tab.insert(Obj.Con, constDef.getConstName(), constDef.getConstVal().obj.getType());
				obj.setAdr(constDef.getConstVal().obj.getAdr());
				obj.setLevel(0);
			}
			else {
				report_error("Greska na " + constDef.getLine() + "(" + constDef.getConstName() + ") vec deklarisano", null);
			}
		} 
		else {
			report_error("Greska na " + constDef.getLine() + ", nekompatibilni tipovi", null);
		}
		 
	}

	public void visit(ConstNum constNum) {
        constNum.obj = new Obj(Obj.Con, "", Tab.intType, constNum.getNum(), Obj.NO_VALUE);	//ovde postavimo da ce obj cvor konstante imati u polju za adresu vrednost same konstante	
    }
	
	public void visit(ConstChar constChar) {
       constChar.obj = new Obj(Obj.Con, "", Tab.charType, constChar.getChr(), Obj.NO_VALUE);
    }
	
	public void visit(ConstBool constBool) {
       constBool.obj = new Obj(Obj.Con, "", SymTabExtender.boolType, Boolean.valueOf(constBool.getBool()) ? 1 : 0, Obj.NO_VALUE);
    }
	
		
	public void visit(VarDeclDefineSingle varDecl) {		
		if (Tab.currentScope().findSymbol(varDecl.getName()) == null) {		
			report_info("Deklarisana promenljiva "+ "'" + varDecl.getName() + "'" + " tipa " + curType, varDecl);
			varDecl.obj = Tab.insert(Obj.Var, varDecl.getName(), currentType);
			//varDecl.obj.setAdr(0);
			if (varDecl.obj.getType().getKind()==Struct.Enum) {				
				localEnumsMap.put(varDecl.getName(), curType);		//pamtim za enumsku promeljivu koji je njen tip; treba mi za proveru dodele
			}
		}
		else {
			report_error("Greska na liniji " + varDecl.getLine() + " :promenljiva " + varDecl.getName() + " je vec deklarisana", varDecl);
		}
		globalVarCnt++;
        
	}
	
	public void visit(VarDeclDefineArray varDecl) {
		if (Tab.currentScope().findSymbol(varDecl.getName()) == null) {		
			//Obj obj = Tab.insert(Obj.Var, varDecl.getName(), currentType);
			varDecl.obj = Tab.insert(Obj.Var, varDecl.getName(), new Struct(Struct.Array, currentType));
			//varDecl.obj.setAdr(1);
			report_info("Deklarisana promenljiva niza "+ "'" + varDecl.getName() + "'" + " tipa " + currentType.getKind(), varDecl);
		}
		else {
			report_info("Greska na liniji" + varDecl.getLine() + " :promenljiva " + varDecl.getName() + " je vec deklarisana", varDecl);
		}
		globalArrayCnt++;
	}
	
	public void visit(GlobalVarDecl varDecl) {      
        globalVarCnt++;
    }
	
	public void visit(VarDecl varDecl) {      
        mainLocalVarCnt++;
    }
	
	
	public void visit(EnumDecl enumDecl) {
		Tab.chainLocalSymbols(currentEnum);
		Tab.closeScope();
		enumVal = 0;
		currentEnum = null;
	}
	
	public void visit(EnumName enumDecl) { 
		if (Tab.currentScope().findSymbol(enumDecl.getId()) == null) {
			enumDecl.obj = Tab.insert(Obj.Type, enumDecl.getId(), new Struct(Struct.Enum));
			Tab.openScope();			
			enumVal = 0;
			currentEnum = enumDecl.obj;
			report_info("Deklarisan novi enum tip " + enumDecl.getId(), enumDecl);
		}
		else {
			enumDecl.obj = Tab.noObj;
			report_error("Greska: Enum tip " + enumDecl.getId() + " je vec deklarisan", enumDecl);
		}
	}
	
	public void visit(EnumDeclDefNoVal enumDecl) {
		if (Tab.currentScope().findSymbol(currentEnum.getName()+"."+enumDecl.getName()) == null) {			
			enumDecl.obj = Tab.insert(Obj.Con, currentEnum.getName()+"."+enumDecl.getName(), Tab.intType);
			report_info("Deklarisana konstanta " +currentEnum.getName()+"."+enumDecl.getName() +" unutar enuma " + currentEnum.getName(), enumDecl);
			enumDecl.obj.setAdr(enumVal++);	
			enumMap.put(currentEnum.getName()+"."+enumDecl.getName(), currentEnum);
		}
		else {
			enumDecl.obj = Tab.noObj;
			report_error("Greska: Konstanta " + enumDecl.getName() + " vec postoji unutar enuma " + currentEnum.getName() , enumDecl);
		}
	}
	
	public void visit(EnumDeclDefVal enumDecl) {
		if (Tab.currentScope().findSymbol(currentEnum.getName()+"."+enumDecl.getName()) == null) {
			enumDecl.obj = Tab.insert(Obj.Con, currentEnum.getName()+"."+enumDecl.getName(), Tab.intType);
			enumDecl.obj.setAdr(enumDecl.getNum());
			report_info("Deklarisana konstanta " + currentEnum.getName()+"."+enumDecl.getName() +" unutar enuma " + currentEnum.getName(), enumDecl);
			enumVal = enumDecl.getNum() + 1;
			enumMap.put(currentEnum.getName()+"."+enumDecl.getName(), currentEnum);
		}
		else {
			report_error("Greska: Konstanta " + enumDecl.getName() + " vec postoji unutar enuma " + currentEnum.getName() , enumDecl);
		}
	}
	
	public void visit(Type type){
    	Obj typeNode = Tab.find(type.getTypeName());
    	if(typeNode == Tab.noObj && (typeNode.getType()!=SymTabExtender.boolType)){						//ako nije nadjen, vratice se noObj
    		report_error("Nije pronadjen tip " + type.getTypeName() + " u tabeli simbola! ", null);
    		type.struct = Tab.noType;
    	}else{
    		if(Obj.Type == typeNode.getKind()){			//proveravamo da li je typeNode koji je vracen po tipu Type 
    			type.struct = typeNode.getType();
    		}else{				//ako nije, znaci da je vracen obj cvor koji nije type nego ko zna sta, to je greska
    			report_error("Greska: Ime " + type.getTypeName() + " ne predstavlja tip!", type);
    			type.struct = Tab.noType;
    		}
    	}
    	currentType = typeNode.getType();
    	curType = type.getTypeName();	//pamtim zbog mape lokalnih enum promenljivih
    }
	
	public void visit(VoidableType voidableType) {
		voidableType.obj = Tab.noObj;
	}
	
	public void visit(MethodDecl methodDecl) {
		Tab.chainLocalSymbols(currentMethod);
		Tab.closeScope();
		currentMethod = Tab.noObj;
	}	
	
	public void visit(MethodSignature methodSignature) {
		currentMethod = Tab.noObj;
		if (Tab.currentScope().findSymbol(methodSignature.getI2()) == null) {
			currentMethod = Tab.insert(Obj.Meth, methodSignature.getI2(), methodSignature.getVoidableType().obj.getType());
			methodSignature.obj = currentMethod;
			
			if (currentClass.equals(Tab.noObj) && "main".equalsIgnoreCase(currentMethod.getName())) {
                mainFound = true;

                if (!currentMethod.getType().equals(Tab.noType)) {
                    report_error("Greska na " + methodSignature.getLine() + ": return vrednost metode main mora biti void", null);
                }
            }
			Tab.openScope();
			report_info("Obradjuje se funkcija " + methodSignature.getI2() + " na liniji " + methodSignature.getLine(), null);
		}
	}

	public void visit(DesignStatAssignment assign) {
		Obj desObj = assign.getDesignator().obj;
		int kind = desObj.getKind();
		
		
		if (kind != Obj.Var && kind != Obj.Elem && kind != Obj.Fld) {
			report_error("Greska na " + assign.getLine() + ": neispravna leva strana dodele", null);
		}	
		else if (kind == Obj.Elem) {			//ako je leva strana element niza
			
			if (desObj.getType().getKind()==1 && assign.getAssignement().obj.getType().getKind()==6) {		//ako je desno enum
				report_info("Dodeljenja vrednost enum i int", null);
			}
			else if(desObj.getType().getKind()==6 && assign.getAssignement().obj.getType().getKind()==1) {
				report_info("Indeks je enum, dodela je int", null);
			}
			else if (!assign.getAssignement().obj.getType().assignableTo(desObj.getType())) {			
				 report_error(" OVDE Greska: nekompatibilni tipovi u dodeli", assign);
				// report_error(desObj.getType().getKind() + " je kind designatora, " + assign.getAssignement().obj.getType().getKind() + " je kind assignmenta", null);		 
			} 
		} 
		else if (assign.getAssignement().obj.getType().getKind() == 3) {		//ako je desna strana element niza
			if (kind == Obj.Elem) {		//ako je i leva strana element niza
				if (!assign.getAssignement().obj.getType().getElemType().assignableTo(desObj.getType().getElemType())) {
					report_error("Greska: nekompatibilni tipovi pri dodeli elementa niza drugom elementu niza", assign);
				}
			} 
			else if (desObj.getType().getKind() == 3) {		//ako je levo promenljiva niza, ne element
				if (!assign.getAssignement().obj.getType().getElemType().assignableTo(desObj.getType().getElemType())){
					report_error("Greska: nekompatabilan tip pri kreiranju novog niza", assign);				
				}
			}
			/*else if (!assign.getAssignement().struct.getElemType().assignableTo(desObj.getType())) {	//ako levo nije niz, nego obicna promenljiva
				report_error("Greska: nekompatibilni tipovi pri dodeli elementa niza promenljivoj", assign);
			}*/
		}
		else if (desObj.getType().getKind()==6 && assign.getAssignement().obj.getType().getKind()==1) {		//kada enum promenljivoj dodeljujemo enum konstantu, npr Broj jedan=Broj.JEDAN, proveravamo da li su obe strane isti enum
			if (!checkEnums(desObj,assign.getAssignement().obj)) {
				report_error("Greska: Ne moze se jedan enum tip dodeliti drugom enum tipu", assign);
			}
		}
		else if (!assign.getAssignement().obj.getType().assignableTo(desObj.getType())) {		//za dodele promenljivih, bez nizova		
			 report_error("Greska: nekompatibilni tipovi u dodeli", assign);
			 report_error(desObj.getType().getKind() + " je kind designatora, " + assign.getAssignement().obj.getType().getKind() + " je kind assignmenta", null);		 
		}
		else {
			report_info("Izvresna dodela vrednosti", null);		// + desObj.getType().getKind() + " je design," + assign.getAssignement().obj.getType().getKind() + " je assign"
		}
		
	}
	
	public void visit(AssignementExpr assign) {
		assign.obj = assign.getExpr().obj;
	}
	
	public void visit(AssignementExprError assign) {
		assign.obj = Tab.noObj;
	}
	
	
	public void visit(DesignStatInc design) {
		Obj designObj = design.getDesignator().obj;
        int kind = designObj.getKind();
        
        if (kind != Obj.Var && kind != Obj.Elem && kind != Obj.Fld) {
        	report_error("Greska: " + designObj.getName() + " nije promenljiva",  design);
        }
        else if(!designObj.getType().equals(Tab.intType)) {
        	report_error("Greska: " + designObj.getName() + " nije int", design);
        }
        else {
        	report_info("Inkrementirana vrednost", null);
        }
	}
	
	public void visit(DesignStatDec design) {
		Obj designObj = design.getDesignator().obj;
        int kind = designObj.getKind();
        
        if (kind != Obj.Var && kind != Obj.Elem && kind != Obj.Fld) {
        	report_error("Greska: " + designObj.getName() + " nije promenljiva", design);
        }
        else if(!designObj.getType().equals(Tab.intType)) {
        	report_error("Greska: " + designObj.getName() + " nije int", design);
        }
        else {
        	report_info("Dekrementirana vrednost", null);
        }
	}
	
	public void visit(StatRead statRead) {
		Obj designObj = statRead.getDesignator().obj;
		int kind = designObj.getKind();
		
		if (kind != Obj.Var && kind != Obj.Elem && kind != Obj.Fld) {
            report_error("Greska: parametar read funkcije nije promenljiva", statRead);
        } else if (!designObj.getType().equals(Tab.intType) && !designObj.getType().equals(Tab.charType) && !designObj.getType().equals(SymTabExtender.boolType)) {
            report_error("Greska: parametar read funkcije je nekorektnog tipa", statRead);
        }
        else {
        	report_info("Read funkcija", statRead);
        	readCallCount++;
        }
		
	}
	
	public void visit(StatPrint statPrint) {
		Obj parType = statPrint.getExpr().obj;
		
		if (parType.getKind() == Struct.Array) {
			if (!parType.getType().getElemType().equals(Tab.intType) && !parType.getType().getElemType().equals(Tab.charType) && !parType.getType().getElemType().equals(SymTabExtender.boolType)) {
	            report_error("Greska: parametar print funkcije nije validan", statPrint);
	        }
		}		
		else if (!parType.getType().equals(Tab.intType) && !parType.getType().equals(Tab.charType) && !parType.getType().equals(SymTabExtender.boolType)) {
            report_error("Greska: parametar print funkcije nije validan", statPrint);
        }
		else {
			report_info("Print funkcija", statPrint);
			printCallCount++;
		}
	}

	public void visit(StatPrintParam statPrint) {	//u ovoj smeni broj predstavlja broj razmaka od prethodnog znaka
		Obj parType = statPrint.getExpr().obj;		
		
		if (parType.getKind() == Struct.Array) {
			if (!parType.getType().getElemType().equals(Tab.intType) && !parType.getType().getElemType().equals(Tab.charType) && !parType.getType().getElemType().equals(SymTabExtender.boolType)) {
	            report_error("Greska: parametar print funkcije nije validan", statPrint);
	        }
		}
		else if (!parType.getType().equals(Tab.intType) && !parType.getType().equals(Tab.charType) && !parType.getType().equals(SymTabExtender.boolType)) {
            report_error("Greska: parametar print funkcije nije validan", statPrint);
        }
		else {
			report_info("Print funkcija", statPrint);
			printCallCount++;
		}
	}
	
	public void  visit(AddopExp addopExpr) {		
		//if(!addopExpr.getExpr().struct.compatibleWith(addopExpr.getTerm().struct)) {
		if (!addopExpr.getExpr().obj.getType().equals(Tab.intType) && addopExpr.getExpr().obj.getType().getKind()!=6 && !addopExpr.getTerm().equals(Tab.intType) && addopExpr.getTerm().obj.getType().getKind()!=6) {	
			report_error(addopExpr.getExpr().obj.getType().getKind() + "je kind expr, " + addopExpr.getTerm().obj.getType().getKind() + " je kind terma" , null);
			report_error("Greska: clanovi izraza nisu korektnog tipa" , addopExpr);
		}
		if (addopExpr.getExpr().obj.getType().getKind() == 1) {		//ako je levi operand int, proslediti njegov tip
			addopExpr.obj = addopExpr.getExpr().obj;
		}
		else {
			addopExpr.obj = addopExpr.getTerm().obj;		//ako levi operand nije int, onda je enum, proslediti tip desnog koji je int
		}
		
	}
	
	public void visit(TermExpr termExpr) {
		termExpr.obj = termExpr.getTerm().obj;
		forIndex=false;
	}
	
	public void  visit(NegTermExpr negExpr) {
		if (forIndex) {
			forIndex=false;
			report_error("Greska: Indeks niza ne sme biti negativan", negExpr);
			negExpr.obj = Tab.noObj;
		}
		else if (!negExpr.getTerm().obj.getType().equals(Tab.intType) && negExpr.getTerm().obj.getType().getKind()!=6){
			report_error("Greska: negira se clan koji nije int" , negExpr);
		}
		negExpr.obj = negExpr.getTerm().obj;
	}
	
	public void visit(TermFact termFact) {
		termFact.obj = termFact.getFactor().obj;
	}
	
	public void visit(TermMulop termMulop) {
		
		if (termMulop.getTerm().obj.getType().getKind() == 3){
			if (termMulop.getTerm().obj.getType().getElemType().getKind()!=1) {
				report_error("Greska: clanovi izraza nisu korektnog tipa", termMulop);
			}
		} else if (termMulop.getFactor().obj.getType().getKind() == 3) {
			if (termMulop.getFactor().obj.getType().getElemType().getKind()!=1) {
				report_error("Greska: clanovi izraza nisu korektnog tipa", termMulop);
			}
		}else if (!termMulop.getTerm().obj.getType().equals(Tab.intType) && termMulop.getTerm().obj.getType().getKind()!=6 && !termMulop.getFactor().obj.getType().equals(Tab.intType) && termMulop.getFactor().obj.getType().getKind()!=6 ) {
            report_error("Greska: clanovi izraza nisu korektnog tipa", termMulop);
        }
		
		if (termMulop.getTerm().obj.getType().getKind() == 1) {		//ako je levi operand int, proslediti njegov tip
			termMulop.obj = termMulop.getTerm().obj;
		}
		else {
			termMulop.obj = termMulop.getFactor().obj;		//ako levi operand nije int, onda je enum, proslediti tip desnog koji je int
		}	
	}
	
	public void visit(FactorExpr factorExpr) {
		factorExpr.obj = factorExpr.getExpr().obj;
	}
	
	public void visit(FactorConst factorConst) {
		factorConst.obj = factorConst.getConstVal().obj;
	}
	
	public void visit(FactorNewArr factorNewArr) {
		if (!factorNewArr.getExpr().obj.getType().equals(Tab.intType)) {
			report_error("Greska: izraz unutar zagrada nije int", factorNewArr);
		}
		
		//factorNewArr.struct = factorNewArr.getExpr().struct; - ovo bi na tip stavilo tip indeksa, a ne tip niza
		//factorNewArr.struct = currentType;		
		Struct struct = new Struct(Struct.Array, currentType);	
		factorNewArr.obj = new Obj(Obj.Type, "newArr", struct);
	}
	
	
	 public void visit(FactorDesign factorDesign) {
		/*Obj designObj = factorDesign.getDesignator().obj;			NEPOTREBNE PROVERE
		int kind = designObj.getKind();		
		if (kind!=Obj.Con && kind != Obj.Var && kind != Obj.Elem && kind!=Obj.Type) {
			report_error(kind  + " je kind", null);
			report_error("Greska: " + designObj.getName() + " nije promenljiva", factorDesign);
		}*/
		 
		factorDesign.obj = factorDesign.getDesignator().obj;
		//report_info("factor type je " + factorDesign.struct.getKind(), factorDesign);
	}
	 
	public void visit(DesIdent desIdent) {
		desIdent.obj = Tab.find(desIdent.getId());
		if (desIdent.obj.equals(Tab.noObj)) {
			report_error("Greska: " + desIdent.getId() + " nije pronadjen", desIdent);
		}
	}
	
	public void visit(DesEnum desEnum) {
		Obj enumType = Tab.find(desEnum.getI1());
		
		boolean found = false;
		desEnum.obj = Tab.noObj;
		//report_info("Enum tip je " + desEnum.getI1() + ", a kind mu je " + enumType.getKind(), null);
		if (!enumMap.containsValue(enumType)) {
			report_error("Ne postoji enum tip " + desEnum.getI1(), desEnum);
		} 
		if (enumType.getType().getKind() == Struct.Enum) {
			//report_info("USAO " + enumType.getKind(), null);
			Iterator<Obj> it = enumType.getLocalSymbols().iterator();
			while (it.hasNext() && !found) {
				Obj temp = it.next();
				if (temp.getName().equals(enumType.getName()+"."+desEnum.getIded())) {
					found = true;
					//desEnum.obj = temp;
					desEnum.obj = new Obj(Obj.Con, enumType.getName()+"."+desEnum.getIded(), temp.getType(), temp.getAdr(),Obj.NO_VALUE);
				}
			}
			if (!found) {
				report_error("Greska: U enumu " + enumType.getName() + " ne postoji vrednost za " +desEnum.getIded(), desEnum);
			}
		}
		
	}
	
	public void visit(DesArr desArr) {
		
		
		if (Tab.noObj.equals(desArr.getArrayName().obj)) {
			//report_info("USAO",null);
			desArr.obj = Tab.noObj;
			report_error("Greska: promenljiva kojoj pristupate nije deklarisana", desArr);
		} 
		else if (desArr.getArrayName().obj.getType().getKind() != Struct.Array) {
			desArr.obj = Tab.noObj;
			report_error("Greska: promenljiva " + desArr.obj.getName() + " nije niz", desArr);
		}
		else if (!desArr.getExpr().obj.getType().equals(Tab.intType) && desArr.getExpr().obj.getType().getKind()!=6) {
			desArr.obj = Tab.noObj;
			report_error("Greska: indeks u zagradama nije int", desArr);
		}
		else {
			desArr.obj = new Obj(Obj.Elem, desArr.getArrayName().obj.getName(), desArr.getArrayName().obj.getType().getElemType());		//uzimamo tip elemenata niza, to smo deklarisali kad smo stavili niz u tabelu simbola pri deklaraciji
		}
	}
	
	public void visit(ArrayName arrName) {
		arrName.obj = Tab.find(arrName.getId());
		if (arrName.obj.equals(Tab.noObj)) {
			arrName.obj = Tab.noObj;
			report_error("Greska: " + arrName.getId() + " nije pronadjen", arrName);
		}
		forIndex=true;		//da bih ispitala da li je negativan index, ovo pamtim pre nego sto pristupim izrazu koji je indeks
	}
	
	
	
}

